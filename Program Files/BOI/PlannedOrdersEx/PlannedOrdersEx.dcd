<?xml version="1.0"?>
<!-- 
****************************************************************************
*  %name:           PlannedOrdersEx.dcd %
*  %version:        1 %
*  %created_by:     groussea %
*  %derived_by:     groussea %
*  %date_modified:  2006/08/03 14:10:46 %
****************************************************************************
-->
<DCD>
    <ElementDef Model="Elements" Id="PlannedOrdersEx" Type="cprrp100" Root="True" 
                Description="Planned Orders" JavaPackage="com.baan.boi.enterpriseplanning">
        <Group Occurs="Required">
        
            <Element>Planner</Element>
			<Element>Buyer</Element>
            <Element>DestinationWarehouse</Element>
            
            <Element>Carrier</Element>
            <Element>FixedFinishDate</Element>
            <Element>FixedStartDate</Element>
            <Element>PlanItem</Element>
            <Element>OrderFinishDate</Element>
            <Element>Routing</Element>
            <Element>PlannedOrder</Element>
            <Element>OrderStatus</Element>
            <Element>PeggedSite</Element>
            <Element>PeggedOrderType</Element>
            <Element>PeggedOrderNumber</Element>
            <Element>PeggedPositionNumber</Element>
            <Element>PeggedSequenceNumber</Element>
            <Element>PlannedByPlanner</Element>
            <Element>PlannedFinishDate</Element>
            <Element>PlanningMethod</Element>
            <Element>PeggedOperation</Element>
            <Element>Scenario</Element>
            <Element>PlanLevel</Element>
            <Element>PlannedRequirementDate</Element>
            <Element>PlannedStartDate</Element>
            <Element>TotalOrderQuantity</Element>
            <Element>TransactionDate</Element>
            <Element>ToolingUpdated</Element>
            <Element>OrderType</Element>
            <Element>Supplier</Element>
            <Element>SupplierShipFrom</Element>
            <Element>SupplyingItem</Element>
            <Element>SupplyingSite</Element>
            <Element>SupplyingWarehouse</Element>
			<Element>IsSubcontracted</Element>

            <!-- Plan Item -->
            <Element>DefaultWarehouse</Element>
            <Element>EnterpriseUnit</Element>
            <Element>Cluster</Element>
            
            <!-- Planned Inventory Movements -->
            <Element>Date</Element>
            <Element>Quantity</Element>
            
            <Group Occurs="ZeroOrMore">
                <Element Reference="True">PlannedCapacityUseEx</Element>
            </Group>     
            
            <Group Occurs="ZeroOrMore">
                <Element Reference="True">PlannedInventoryMovementsByItemEx</Element>
            </Group>          

            <Group Occurs="ZeroOrMore">
                <Element Reference="True">PlannedOrderLineEx</Element>
            </Group>
        </Group>
    </ElementDef>

    <!-- PlanItem -->
    <ElementDef Datatype="string" Model="Data" Id="DefaultWarehouse" Type="dcwar" Length="6" Description="Default Warehouse"/>
    <ElementDef Datatype="string" Model="Data" Id="EnterpriseUnit" Type="grid" Length="6" Description="Enterprise Unit"/>
    <ElementDef Datatype="string" Model="Data" Id="Cluster" Type="clus" Length="3" Description="Cluster"/>
    
    <!-- Planned Inventory Movements -->
    <ElementDef Id="Date" Type="date" Description="Date" Model="Data" Datatype="dateTime" />
    <ElementDef Id="Quantity" Type="qana" Description="Quantity" Model="Data" Datatype="r8" />

	<ElementDef Datatype="string" Model="Data" Id="Buyer" Type="buyr" Length="9" Description="Buyer"/>
    <ElementDef Datatype="string" Model="Data" Id="Planner" Type="cplb" Length="9" Description="Planner"/>
    <ElementDef Datatype="string" Model="Data" Id="DestinationWarehouse" Type="dwar" Length="6" Description="Destination Warehouse"/>
    <ElementDef Datatype="dateTime" Model="Data" Id="FixedFinishDate" Type="ffdt" Description="Fixed Finish Date"/>
    <ElementDef Datatype="dateTime" Model="Data" Id="FixedStartDate" Type="fsdt" Description="Fixed Start Date"/>
    <ElementDef Datatype="string" Model="Data" Id="PlanItem" Type="item" Length="50" Description="Plan Item(Segmented)(Plan)"/>
    <ElementDef Datatype="dateTime" Model="Data" Id="OrderFinishDate" Type="ofdt" Description="Order finish date"/>
    <ElementDef Datatype="string" Model="Data" Id="Routing" Type="opro" Length="6" Description="Routing"/>
    <ElementDef Datatype="string" Model="Data" Id="PlannedOrder" Type="orno" Length="9" Description="Planned Order"/>
    <ElementDef Datatype="ui1" Model="Data" Id="OrderStatus" Type="osta" Description="Order Status"/>
    <ElementDef Datatype="ui1" Model="Data" Id="PlannedByPlanner" Type="pbpl" Description="Planned by Planner"/>
    <ElementDef Datatype="dateTime" Model="Data" Id="PlannedFinishDate" Type="pfdt" Description="Planned Finish Date"/>
    <ElementDef Datatype="ui1" Model="Data" Id="PlanningMethod" Type="plmt" Description="Planning Method"/>
    <ElementDef Datatype="string" Model="Data" Id="Scenario" Type="plnc" Length="3" Description="Scenario"/>
    <ElementDef Datatype="ui1" Model="Data" Id="PlanLevel" Type="plvl" Description="Plan Level"/>
    <ElementDef Datatype="dateTime" Model="Data" Id="PlannedRequirementDate" Type="prdt" Description="Planned Requirement Date"/>
    <ElementDef Datatype="dateTime" Model="Data" Id="PlannedStartDate" Type="psdt" Description="Planned Start Date"/>
    <ElementDef Datatype="r8" Model="Data" Id="TotalOrderQuantity" Type="quan" Description="Total order quantity"/>
    <ElementDef Datatype="dateTime" Model="Data" Id="TransactionDate" Type="trdt" Description="Transaction Date"/>
    <ElementDef Datatype="ui1" Model="Data" Id="ToolingUpdated" Type="trpu" Description="Tooling Updated"/>
    <ElementDef Datatype="ui1" Model="Data" Id="OrderType" Type="type" Description="Order Type"/>
    <ElementDef Datatype="ui1" Model="Data" Id="IsSubcontracted" Type="subc" Description="Is Subcontracted"/>

    <!--- Planned Order Lines -->
    <ElementDef Model="Elements" Id="PlannedOrderLineEx" Type="cprrp110" Root="True" Description="Planned Order Line" 
                JavaPackage="com.baan.boi.enterpriseplanning">
        <Group Occurs="Required">
            <Element>Carrier</Element>
            <Element>OrderLineType</Element>
            <Element>LinesPlanItem</Element>
            <Element>OrderLineFinishDate</Element>
            <Element>OrderLineStartDate</Element>
            <Element>ReferenceDate</Element>            
            <Element>ItemRouting</Element>
            <Element>OrderQuantity</Element>
            <Element>PlannedOrder</Element>
            <Element>PeggedOrderType</Element>
            <Element>PeggedOperation</Element>
            <Element>PeggedOrderNumber</Element>
            <Element>PeggedPositionNumber</Element>
            <Element>PeggedSequenceNumber</Element>
            <Element>PeggedSite</Element>
            <Element>Scenario</Element>
            <Element>PlanLevel</Element>
            <Element>PositionNumber</Element>
            <Element>Revision</Element>
            <Element>ScheduleCode</Element>
            <Element>SchedulePosition</Element>
            <Element>Supplier</Element>
            <Element>SupplyingItem</Element>
            <Element>SupplierShipFrom</Element>
            <Element>SupplyingSite</Element>
            <Element>SupplyingWarehouse</Element>
            <Element>PlannedByPlanner</Element>
            
            <Group Occurs="ZeroOrMore">
                <Element Reference="True">PlannedCapacityUseEx</Element>
            </Group>
        </Group>
    </ElementDef>

    <ElementDef Datatype="dateTime" Model="Data" Id="ReferenceDate" Type="rfdt" Description="Reference Date"/>
    
    <!-- PlanItem -->
    <ElementDef Datatype="string" Model="Data" Id="SupplyingDefaultWarehouse" Type="sdcwar" Length="6" Description="Default Warehouse"/>
    <ElementDef Datatype="string" Model="Data" Id="SupplyingEnterpriseUnit" Type="sgrid" Length="6" Description="Enterprise Unit"/>

    <ElementDef Datatype="ui1" Model="Data" Id="OrderLineType" Type="koor" Description="Order Line Type"/>
    <ElementDef Datatype="string" Model="Data" Id="Carrier" Type="carr" Length="3" Description="Carrier"/>
    <ElementDef Datatype="dateTime" Model="Data" Id="OrderLineFinishDate" Type="olfd" Description="Order Line Finish Date"/>
    <ElementDef Datatype="dateTime" Model="Data" Id="OrderLineStartDate" Type="olsd" Description="Order Line Start Date"/>
    <ElementDef Datatype="string" Model="Data" Id="ItemRouting" Type="opro" Length="6" Description="Item Routing"/>
    <ElementDef Datatype="r8" Model="Data" Id="OrderQuantity" Type="oqan" Description="Order Quantity"/>
    <ElementDef Datatype="ui1" Model="Data" Id="PeggedOrderType" Type="peko" Description="Pegged Order Type"/>
    <ElementDef Datatype="i2" Model="Data" Id="PeggedOperation" Type="peop" Description="Pegged Operation"/>
    <ElementDef Datatype="string" Model="Data" Id="PeggedOrderNumber" Type="peor" Length="9" Description="Pegged Order Number"/>
    <ElementDef Datatype="i2" Model="Data" Id="PeggedPositionNumber" Type="pepo" Description="Pegged Position Number"/>
    <ElementDef Datatype="i2" Model="Data" Id="PeggedSequenceNumber" Type="pese" Description="Pegged Sequence Number"/>
    <ElementDef Datatype="i2" Model="Data" Id="PeggedSite" Type="pesi" Description="Pegged Site"/>
    <ElementDef Datatype="i2" Model="Data" Id="PositionNumber" Type="pono" Description="Position Number"/>
    <ElementDef Datatype="i2" Model="Data" Id="Revision" Type="revn" Description="Revision"/>
    <ElementDef Datatype="string" Model="Data" Id="ScheduleCode" Type="schn" Length="9" Description="Schedule Code"/>
    <ElementDef Datatype="i2" Model="Data" Id="SchedulePosition" Type="spon" Description="Schedule Position"/>
    <ElementDef Datatype="string" Model="Data" Id="Supplier" Type="suno" Length="9" Description="Supplier"/>
    <ElementDef Datatype="string" Model="Data" Id="SupplyingItem" Type="supl" Length="50" Description="Supplying Item(Segmented)(Plan)"/>
    <ElementDef Datatype="string" Model="Data" Id="SupplierShipFrom" Type="sush" Length="9" Description="Supplier (ship-from)"/>
    <ElementDef Datatype="i2" Model="Data" Id="SupplyingSite" Type="susi" Description="Supplying Site"/>
    <ElementDef Datatype="string" Model="Data" Id="SupplyingWarehouse" Type="suwa" Length="6" Description="Supplying Warehouse"/>
    <ElementDef Datatype="string" Model="Data" Id="LinesPlanItem" Type="litm" Length="50" Description="Plan Item(Segmented)(Plan)"/>
</DCD>
