<?xml version="1.0"?>
<!-- 
****************************************************************************
*  %name:           PlannedOrdersEx.qcd %
*  %version:        1 %
*  %created_by:     groussea %
*  %derived_by:     groussea %
*  %date_modified:  2016/09/15 %
****************************************************************************
-->
<QCD>
    <ManagerDef Type="PlannedOrdersExManager" JavaPackage="com.baan.boi.enterpriseplanning">
        <Query>Show</Query>
        <Query>ListPlannedPurchaseOrdersForPlanner</Query>
        <Query>ListPlannedDistributionOrdersForPlanner</Query>
        <Query>ListPlannedManufacturingOrdersForPlanner</Query>
    </ManagerDef>

    <ElementDef Datatype="string" Model="Data" Type="Scenario" Length="3"/>
    <ElementDef Datatype="ui1" Model="Data" Type="FromOrderType"/>
    <ElementDef Datatype="ui1" Model="Data" Type="ToOrderType"/>
    <ElementDef Datatype="string" Model="Data" Type="FromPlannedOrder" Length="9"/>
    <ElementDef Datatype="string" Model="Data" Type="ToPlannedOrder" Length="9"/>
    <ElementDef Datatype="string" Model="Data" Type="FromItem" Length="50"/>
    <ElementDef Datatype="string" Model="Data" Type="ToItem" Length="50"/>
    <ElementDef Datatype="ui1" Model="Data" Type="osta" Id="FromOrderStatus" Description="From Order Status" />
    <ElementDef Datatype="ui1" Model="Data" Type="osta" Id="ToOrderStatus" Description="To Order Status" />
    <ElementDef Datatype="string" Model="Data" Type="FromPlanner" Length="9" Description="From Planner"/>
    <ElementDef Datatype="string" Model="Data" Type="ToPlanner" Length="9" Description="To Planner"/>
    <ElementDef Datatype="string" Model="Data" Type="FromBuyer" Length="9" Description="From Buyer"/>
    <ElementDef Datatype="string" Model="Data" Type="ToBuyer" Length="9" Description="To Buyer"/>

    
    <ElementDef Datatype="ui1" Model="Data" Type="FromPlanLevel" Description="Plan Level"/>
    <ElementDef Datatype="ui1" Model="Data" Type="ToPlanLevel" Description="Plan Level"/>
    
    <ElementDef Datatype="dateTime" Model="Data" Type="FromPlannedStartDate" Description="From Planned Start Date" />
    <ElementDef Datatype="dateTime" Model="Data" Type="ToPlannedStartDate" Description="To Planned Start Date" />

    <ElementDef Datatype="ui1" Model="Data" Type="FromOrderStatusE2P" Description="From Order Status" />
    <ElementDef Datatype="ui1" Model="Data" Type="ToOrderStatusE2P" Description="To Order Status" />    

    

    <!-- The Show Method is actually used by Planner! -->
    <QueryDef Language="BaanSQL" Type="Show">
        <QueryText>
            SELECT  cprrp100.orno
            FROM    cprrp100 
            WHERE   cprrp100._index1 = {:Scenario, :OrderType, :PlannedOrder}
        </QueryText>
        <Parameters>
            <Element>Scenario</Element>
            <Element>OrderType</Element>
            <Element>PlannedOrder</Element>
        </Parameters>
        <Return>
            <Element>PlannedOrdersEx</Element>
        </Return>
    </QueryDef>

    <QueryDef Language="BaanSQL" Type="ListForPlanner">
        <QueryText>
            SELECT  cprrp100.orno, cprrp100.type
            FROM    cprrp100 
            WHERE   cprrp100._index1 INRANGE {:Scenario,:FromOrderType,:FromPlannedOrder} AND {:Scenario,:ToOrderType,:ToPlannedOrder}
        </QueryText>
        <Parameters>
            <Element>Scenario</Element>
            <Element>FromOrderType</Element>
            <Element>ToOrderType</Element>
            <Element>FromPlannedOrder</Element>
            <Element>ToPlannedOrder</Element>
        </Parameters>
        <Return>
            <Element Occurs="ZeroOrMore">PlannedOrdersEx</Element>
        </Return>
    </QueryDef>


    <ElementDef RelatedColumn="type" Datatype="ui1" Model="Data" Type="OrderType" RelatedTable="cprrp100"/>
    <ElementDef RelatedColumn="orno" Datatype="string" Model="Data" Type="PlannedOrder" Length="9" RelatedTable="cprrp100"/>
        
    <QueryDef Language="BaanSQL" Type="ListPlannedPurchaseOrdersForPlanner">
        <QueryText>
            SELECT cprrp010.plnc, cprrp010.koor, cprrp010.orno, cprrp010.date, cprrp010.lpon, cprrp010.pono,
                   cprrp010.qana, cprrp100.osta:osta, cprrp100.cplb:cplb, cprrp100.suno:suno,
                   cprrp100.psdt:psdt, cprrp100.pfdt:pfdt, cprrp100.prdt:prdt, cprrp100.trdt:trdt,
                   cprrp100.sush:sush, tcemm112.grid:grid, cprpd100.plni:item, cprpd100.cwar:dcwar
            FROM   cprrp010
                   join cprrp100 on cprrp010._index1 = {:Scenario, :Company, tckoor.cp.pur, cprrp100.orno}
                   join cprpd100 on cprpd100._index1 = {cprrp010.plni}
                   join tcemm112 on tcemm112._index1 = {:Company, cprpd100.cwar}
            WHERE  
                   cprrp100._index6 INRANGE {:FromItem, :Scenario, tckoor.cp.pur,:FromPlannedOrder} AND {:ToItem, :Scenario, tckoor.cp.pur,:ToPlannedOrder} AND
                   cprrp100.osta INRANGE :FromOrderStatusE2P AND :ToOrderStatusE2P AND
                   <!-- cprrp100.psdt INRANGE :FromPlannedStartDate AND :ToPlannedStartDate AND -->
                   cprrp100.psdt &lt;= :ToPlannedStartDate
        </QueryText>
        <Parameters>
            <Element>FromItem</Element>
            <Element>ToItem</Element>
            <Element>FromPlannedOrder</Element>
            <Element>ToPlannedOrder</Element>
            <Element>FromOrderStatusE2P</Element>
            <Element>ToOrderStatusE2P</Element>
<!--        <Element>FromPlannedStartDate</Element>  -->
            <Element>ToPlannedStartDate</Element>
            <Element>Scenario</Element>
            <Element>Company</Element>
        </Parameters>
        <Return>
            <Element Occurs="ZeroOrMore">PlannedInventoryMovementsByItemEx</Element>
        </Return>
    </QueryDef>
    
    <QueryDef Language="BaanSQL" Type="ListPlannedDistributionOrdersForPlanner">
        <QueryText>
            SELECT cprrp010.plnc, cprrp010.koor, cprrp010.orno, cprrp010.date, cprrp010.lpon, cprrp010.pono,
                   cprrp010.qana, cprrp100.osta:osta, cprrp100.cplb:cplb, cprrp100.psdt:psdt,
                   cprrp100.pfdt:pfdt, cprrp100.prdt:prdt, cprrp100.trdt:trdt,
                   tcemm112.grid:grid, cprpd100.plni:item, cprpd100.cwar:dcwar,
                   cprrp100.psdt:sdate,
                   tcemm112a.grid:sgrid, cprpd100a.plni:sitem, cprpd100a.cwar:sdcwar
            FROM   cprrp010
                   join cprrp100 on cprrp100._index1 = {cprrp010.plnc, cprrp010.koor, cprrp010.orno}
                   join cprpd100 on cprpd100._index1 = {cprrp010.plni}
                   join tcemm112 on tcemm112._index1 = {:Company, cprpd100.cwar}
                   join cprpd100 cprpd100a on cprpd100a._index1 = {cprrp100.supl}
                   join tcemm112 tcemm112a on tcemm112a._index1 = {:Company, cprpd100a.cwar}
            WHERE  cprrp100._index6 INRANGE {:FromItem, :Scenario, tckoor.cp.ipl,:FromPlannedOrder} AND {:ToItem, :Scenario, tckoor.cp.ipl,:ToPlannedOrder} AND
                   cprrp010.kotr = tckotr.receipt AND
                   cprrp100.osta INRANGE :FromOrderStatusE2P AND :ToOrderStatusE2P AND
                   cprrp100.psdt &lt;= :ToPlannedStartDate
        </QueryText>
        <Parameters>
            <Element>FromItem</Element>
            <Element>ToItem</Element>
            <Element>FromPlannedOrder</Element>
            <Element>ToPlannedOrder</Element>
            <Element>FromOrderStatusE2P</Element>
            <Element>ToOrderStatusE2P</Element>
            <Element>ToPlannedStartDate</Element>
            <Element>Scenario</Element>
            <Element>Company</Element>
        </Parameters>
        <Return>
            <Element Occurs="ZeroOrMore">PlannedInventoryMovementsByItemEx</Element>
        </Return>
    </QueryDef>
    
    <QueryDef Language="BaanSQL" Type="ListPlannedManufacturingOrdersForPlanner">
        <QueryText>
            SELECT cprrp100.plnc, cprrp100.type, cprrp100.orno, cprrp100.osta,
                   cprrp100.psdt, cprrp100.pfdt, cprrp100.prdt, cprrp100.trdt,
                   cprrp100.cplb, cprrp100.opro:opro,
                   cprrp100.pfdt:date, cprrp100.quan:qana, tcemm112.grid:grid,
                   cprrp100.item, cprpd100.cwar:dcwar, cprpd100.clus:clus,
                   cprpd100.item, cprrp100.opro
            FROM   cprrp100
                   join cprpd100 on cprpd100._index1 = {cprrp100.item}
                   join tcemm112 on tcemm112._index1 = {:Company, cprpd100.cwar}
            WHERE  cprrp100._index6 INRANGE {:FromItem, :Scenario, tckoor.cp.sfc,:FromPlannedOrder} AND {:ToItem, :Scenario, tckoor.cp.sfc,:ToPlannedOrder} AND
                   cprrp100.osta INRANGE :FromOrderStatusE2P AND :ToOrderStatusE2P AND
                   cprrp100.psdt &lt;= :ToPlannedStartDate
        </QueryText>
        <Parameters>
            <Element>FromItem</Element>
            <Element>ToItem</Element>
            <Element>FromPlannedOrder</Element>
            <Element>ToPlannedOrder</Element>
            <Element>FromOrderStatusE2P</Element>
            <Element>ToOrderStatusE2P</Element>
            <Element>ToPlannedStartDate</Element>
            <Element>Scenario</Element>
            <Element>Company</Element>
        </Parameters>
        <Return>
            <Element Occurs="ZeroOrMore">PlannedOrdersEx</Element>
        </Return>
        <Layout>ManufacturingOrderLayout</Layout>
    </QueryDef>
        
    <LayoutDef Type="cprrp200" Id="PlannedCapacityUseEx"/>
    <LayoutDef Type="cprrp010" Id="PlannedInventoryMovementsByItemEx"/>
    <LayoutDef Type="tirou102" Id="RoutingOperations"/>
    
    <LayoutDef Type="cprrp100" Id="ManufacturingOrderLayout">
        <Layout Reference="True" Query="PlannedCapacityUseForPlanner">
            PlannedCapacityUseEx
        </Layout>
        <Layout Reference="True" Query="PlannedInventoryMovementsByItemForPlanner">
            PlannedInventoryMovementsByItemEx    
        </Layout>
    </LayoutDef>
    
    <LayoutDef Type="cprrp200" Id="CapacityUsedLayout">
        <Layout Reference="True" Query="RoutingOperationsForPlanner">
            RoutingOperations
        </Layout>
    </LayoutDef>

    <QueryDef Language="BaanSQL" Type="PlannedCapacityUseForPlanner">
        <QueryText>
            SELECT   cprrp200.opno, cprrp200.prdt:stdt, cprrp200.fidt, cprrp200.cwoc,
                     cprrp200.fxsu, cprrp200.nopn, cprrp200.wttm, cprrp200.rutm,
                     cprrp200.mccp:prtm, cprrp200.trls, cprrp200.fdur, cprrp200.scpq,
                     cprrp200.yldp, cprrp200.tuni, cprrp200.sutm, tcemm124.grid:cwocgrid,
                     cprrp100.item, cprrp100.prdt, cprrp100.opro
            FROM     cprrp200
                   join cprrp100 on cprrp100._index1 = {:Scenario, :OrderType, :PlannedOrder}
                   join tcemm124 on tcemm124._index1 = {:Company, tctypd.workcentre, cprrp200.cwoc}
            WHERE    cprrp200._index1 = {:Scenario, :OrderType, :PlannedOrder}
        </QueryText>
        <Parameters>
            <Element>Company</Element>
            <Element>Scenario</Element>
            <Element>OrderType</Element> 
            <Element>PlannedOrder</Element>
        </Parameters>
        <Layout>CapacityUsedLayout</Layout> 
    </QueryDef>

    <QueryDef Language="BaanSQL" Type="PlannedInventoryMovementsByItemForPlanner">
        <QueryText>
            SELECT   cprrp010.opno, cprrp010.date, cprrp010.qana, cprrp010.kotr, cprrp010.pono,
                     tcemm112.grid:grid, cprpd100.plni:plni, cprpd100.cwar:dcwar
            FROM     cprrp010
                   join cprpd100 on cprpd100._index1 = {cprrp010.plni}
                   join tcemm112 on tcemm112._index1 = {:Company, cprpd100.cwar}
            WHERE    cprrp010._index1 = {:Scenario, :Company, :OrderType, :PlannedOrder} AND
                     cprrp010.pono != 0
        </QueryText>
        <Parameters>
            <Element>Company</Element>
            <Element>Scenario</Element>
            <Element>OrderType</Element> 
            <Element>PlannedOrder</Element>
        </Parameters>
    </QueryDef>


     
    <QueryDef Language="BaanSQL" Type="RoutingOperationsForPlanner">
        <QueryText>
            SELECT  tirou102.opno, tirou102.seqn,
                    tirou102.rutm, tirou102.mopr, tirou102.mcoc, tirou003.dsca:tdsca
            FROM    tirou102
                    join tirou101 ON tirou101._index1 = {:Item, :Routing}
                    join tirou003 ON tirou003._index1 = {tirou102.tano}
            WHERE  tirou102._index1 = {tirou101.mitm, tirou101.opro, :Operation} AND
                    tirou101.stor = tcyesno.no AND
                    tirou102.indt &lt;= :requirementDate AND
                    ((tirou102.exdt != 0 and tirou102.exdt &gt;= :requirementDate) OR (tirou102.exdt = 0))
        </QueryText>
        <Parameters>
            <Element>Routing</Element>
            <Element>Item</Element>
            <Element>RequirementDate</Element>
            <Element>Operation</Element>
        </Parameters>
    </QueryDef>

    <ElementDef Datatype="i2" Model="Data" Type="Company"/>

    <ElementDef RelatedColumn="prdt" Datatype="dateTime" Model="Data" Type="RequirementDate" RelatedTable="cprrp100"/>
    <ElementDef RelatedColumn="opro" Datatype="string" Model="Data" Type="Routing" Length="6" RelatedTable="cprrp100"/>
    <ElementDef RelatedColumn="item" Datatype="string" Model="Data" Type="Item" Length="47" RelatedTable="cprpd100" Description="Item (Segmented)"/>
    <ElementDef RelatedColumn="opno" Datatype="i2" Model="Data" Type="Operation" RelatedTable="cprrp200"/>
</QCD>
