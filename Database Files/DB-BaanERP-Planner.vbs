'****************************************************************************
' %name:           DB-BaanERP-Planner.vbs %
' %version:        1 %
' %created_by:     groussea %
' %derived_by:     groussea %
' %date_modified:  Thursday, March 13, 2008 10:30:17 %
'****************************************************************************
'*
'*                       NOTICE
'*
'* THIS SOFTWARE IS THE PROPERTY OF AND CONTAINS
'* CONFIDENTIAL INFORMATION OF AAN INTERNATIONAL B.V., 
'* AND SHALL NOT BE COPIED, USED, NOR DISCLOSED
'* WITHOUT EXPRESS WRITTEN AUTHORIZATION.  ALTHOUGH
'* PUBLICATION IS NOT INTENDED, IN THE EVENT OF
'* PUBLICATION, THE FOLLOWING NOTICE IS APPLICABLE:
'*
'* (c) COPYRIGHT 2004 BAAN INTERNATIONAL B.V., ALL RIGHTS RESERVED.
'*
'* BAAN INTERNATIONAL B.V. IS A WHOLLY OWNED SUBSIDIARY OF 
'*             SSA GLOBAL TECHNOLOGIES, INC.
'*
'****************************************************************************
Option Explicit

OpenConnection()

' Find component types and instances
Dim compType1, compType2, compInst1, compInst2
compType1 = FindID("COMPTYPEID", "ITK_COMPTYPE", "NAME = 'ERPln'")
compType2 = FindID("COMPTYPEID", "ITK_COMPTYPE", "NAME = 'Planner'")
compInst1 = FindID("COMPINSTANCEID", "ITK_COMPINSTANCE", "NAME = 'ERPlnDB'")
compInst2 = FindID("COMPINSTANCEID", "ITK_COMPINSTANCE", "NAME = 'PlannerDB'")

'Component type couple
compTypeCouple = CreateComponentTypeCouple(compType1, compType2)

'Component instance couple
compInstCouple = CreateComponentInstanceCouple(compTypeCouple, compInst1, compInst2)

CreateCoupleProperty compTypeCouple, "EPPrefix", "EP", "Prefix for orders coming from EP."
CreateCoupleProperty compTypeCouple, "EPFromStatusFilter", "10", "Specify the minimum order status for the orders to be imported from EP to Planner:" & VbCr & "   10 for planned" & VbCr & "   20 for firmed planned" & VbCr & "   30 for confirmed."

'Class couples
CreateClassCoupleDict "EP Planned Manufacturing Orders", "EnterprisePlanning.PlannedOrdersEx", "BOManufacturingOrder", 1
CreateClassCoupleDict "EP Planned Purchased Orders", "EnterprisePlanning.PlannedInventoryMovementsByItemEx", "BOPurchaseOrder", 1
CreateClassCoupleDict "EP Planned Distribution Orders", "EnterprisePlanning.PlannedInventoryMovementsByItemEx", "BOOrder", 1

'Profiles
profile = CreateProfile("NULL", compInstCouple, "ERPlnToPlanner_Regen_PlannedOrders", "This profile will transfer the planned orders from LN to Planner.")
CreateDataTransferDict "EP Planned Manufacturing Orders",1, "DELETE_DELUNREF",    "TRANSFER_RESYNC", "MATCH_MANUAL"
CreateDataTransferDict "EP Planned Purchased Orders",   1,  "DELETE_DELUNREF",    "TRANSFER_RESYNC", "MATCH_MANUAL"
CreateDataTransferDict "EP Planned Distribution Orders",1,  "DELETE_DELUNREF",    "TRANSFER_RESYNC", "MATCH_MANUAL"

