Dim connP, rsP, id, rank, clsCplDict
Dim compTypeCouple, compInstCouple, profile, group, sysDate, updateInstallSupported

' Direction
Const ITK_DIRECTION_RIGHT = 1
Const ITK_DIRECTION_LEFT  = 2 

' Profile active or not active
Const ITK_PROF_NOT_ACTIVE = 0
Const ITK_PROF_ACTIVE = 1


' ITK database types
Const OLEDB_DBTYPE_INVALID   = "0"
Const OLEDB_DBTYPE_ORACLE    = "1"
Const OLEDB_DBTYPE_SQLSERVER = "2"

' script exit codes; zero cannot be used for success as zero will be returned when a syntax error occurs!
Const EXIT_CODE_SUCCESS = 9999
Const EXIT_CODE_FAILURE = 0

Const DEPRECATED_PREFIX = "ZZ_DEPRECATED_"

Const ERROR_PREFIX = "ERROR: "
Const WARNING_PREFIX = "WARNING: "


' success is return if the script reaches this end point
WScript.Quit EXIT_CODE_SUCCESS




'******************************************************************************
Function CreateAction(prmProfile, prmType, prmEnable, prmDefaultValue)
    WScript.echo "CreateAction:" & prmProfile & "," & prmType & "," & prmEnable & "," & prmDefaultValue 
    id = FindID("ACTIONID", "ITK_ACTION", "PROFILEID = " & prmProfile & " AND RANK = " & rank)
    If IsNull(id) Then
        ' Create ITK_ACTION
        id = GetNewSequenceID("ITK_ACTION")
        ExecuteQuery "INSERT INTO ITK_ACTION (ACTIONID, PROFILEID, RANK, ACTIONTYPE, ACTIONENABLE, DEFAULTVALUE) VALUES (" & _
                     id & ", " & prmProfile & ", " & _
                     rank & ", " & prmType & ", " & prmEnable & ", " & prmDefaultValue & ")"
    End If
    CreateAction = id
    rank = rank + 1
End Function


'******************************************************************************
Function CreateServiceAction(prmCompType, prmProfile, prmEnable, prmServiceName, prmRunType)
    ' prmCompType is not used (as the dependency)  ITKServer doesn't like it!!
    'TODO: error if findID fails..
    WScript.echo "CreateServiceAction:" & prmCompType & "," & prmProfile & "," & prmEnable & "," & prmServiceName & "," & prmRunType

    Dim defaultValue, runTypeValue
    
    ' added this section to establish what the default value is. This value is used to restore values within ITK using bitwise checks
    ' -------------------------------------------------------------------------------------------------------------------------------
    runTypeValue = FindID("TYPENUMBER", "ITK_RUNTYPE", "TYPENAME = '" & prmRunType & "'")
    defaultValue = prmEnable ' 0=disabled, 1=enabled
    defaultValue = defaultValue + 2^(runTypeValue) ' this assumes that the possible values are 1=once, 2=many
    ' -------------------------------------------------------------------------------------------------------------------------------
    
    id = CreateAction(prmProfile, FindID("TYPENUMBER", "ITK_ACTIONTYPE", "TYPENAME = 'ACTION_SERVICE'"), prmEnable, defaultValue)
    If IsNull(FindID("SERVICEID", "ITK_SERVICE", "SERVICEID = " & id)) Then
        ExecuteQuery "INSERT INTO ITK_SERVICE (SERVICEID, COMPTYPEID, COMPTYPEDEPID, NAME, RUNTYPE) VALUES ( " & _
                     id & ", " & prmCompType & ", NULL, '" & prmServiceName & "', " & runTypeValue & ")"
    Else
        ExecuteQuery "UPDATE ITK_SERVICE set " & _
                     "COMPTYPEID=" & prmCompType & ", " & _
                     "COMPTYPEDEPID=" & "NULL" & ", " & _
                     "NAME=" & "'" & prmServiceName & "'" & ", " & _
                     "RUNTYPE=" & runTypeValue & " " & _
                     "where SERVICEID=" & id
    End If
    CreateServiceAction = id
End Function


'******************************************************************************
Function CreateScriptAction(prmCompType1, prmCompType2, prmProfile, prmEnable, prmScriptName, prmRunType)
    WScript.echo "CreateScriptAction:" & prmCompType1 & "," & prmCompType2 & "," & prmProfile & "," & prmEnable & "," & prmScriptName & "," & prmRunType
    Dim scriptID
    Dim defaultValue, runTypeValue
    
    ' added this section to establish what the default value is. This value is used to restore values within ITK using bitwise checks
    ' -------------------------------------------------------------------------------------------------------------------------------
    runTypeValue = FindID("TYPENUMBER", "ITK_RUNTYPE", "TYPENAME = '" & prmRunType & "'")
    defaultValue = prmEnable ' 0=disabled, 1=enabled
    defaultValue = defaultValue + 2^(runTypeValue) ' this assumes that the possible values are 1=once, 2=many
    ' -------------------------------------------------------------------------------------------------------------------------------
    
    scriptID = CreateScript(prmScriptName)
    id = CreateAction(prmProfile, FindID("TYPENUMBER", "ITK_ACTIONTYPE", "TYPENAME = 'ACTION_SCRIPT'"), prmEnable, defaultValue)
    If IsNull(FindID("SCRIPTACTIONID", "ITK_SCRIPTACTION", "SCRIPTACTIONID = " & id)) Then
        ExecuteQuery "INSERT INTO ITK_SCRIPTACTION (SCRIPTACTIONID, COMPTYPEDEP1ID, COMPTYPEDEP2ID, SCRIPTID, RUNTYPE) VALUES (" & _
                     id & ", " & prmCompType1 & ", " & prmCompType2 & ", " & scriptID & ", " & runTypeValue & ")"
    Else
        ExecuteQuery "UPDATE ITK_SCRIPTACTION set " & _
                     "COMPTYPEDEP1ID=" & prmCompType1 & ", " & _
                     "COMPTYPEDEP2ID=" & prmCompType2 & ", " & _
                     "SCRIPTID=" & scriptID & ", " & _
                     "RUNTYPE=" & runTypeValue & " " & _
                     "where SCRIPTACTIONID=" & id
    End If
    CreateScriptAction = id
End Function


'******************************************************************************
Function CreateScript(prmScriptName)
    WScript.echo "CreateScript:" & prmScriptName
    id = FindID("SCRIPTID", "ITK_SCRIPT", "NAME = '" & prmScriptName & "'")
    If IsNull(id) Then
        Dim vbScriptID
        vbScriptID = FindID("SCRIPTLANGUAGEID", "ITK_SCRIPTLANGUAGE", "NAME = 'VB Script'")
        id = GetNewSequenceID("ITK_SCRIPT")
        ExecuteQuery "INSERT INTO ITK_SCRIPT (SCRIPTID, SCRIPTLANGUAGEID, NAME, NOTE, TEXT, TEXTREADONLY, SCRIPTTYPE, MODIFICATIONDATE) VALUES (" & _
                     id & ", " & vbScriptID & ", '" & prmScriptName & "', NULL, NULL, 1, 0, " & sysDate & ")"
    End If
    CreateScript = id
End Function


'******************************************************************************
Sub DeleteScript(prmScriptName, prmScriptType)
    WScript.echo "DeleteScript:" & prmScriptName & "," & prmScriptType
    Dim condition
    If IsNull(prmScriptType) Then
        condition = "NAME = '" & prmScriptName & "'"
    Else
        condition = "NAME = '" & prmScriptName & "' AND SCRIPTTYPE = "  & prmScriptType
    End If
    id = FindID("SCRIPTID", "ITK_SCRIPT", condition)
    If Not IsNull(id) Then
        ExecuteQuery "DELETE FROM ITK_SCRIPT WHERE SCRIPTID = " & id
    End If
End Sub


'******************************************************************************
Function CreateDataTransfer(prmClassCouple, prmProfile, prmEnable, prmDeleteType, prmTransferType, prmMatchType)
    WScript.echo "CreateDataTransfer:" & prmClassCouple & "," & prmProfile & "," & prmEnable & "," & prmDeleteType & "," & prmTransferType & "," & prmMatchType
    Dim defaultValue, deleteTypeValue, transferTypeValue, matchTypeValue, tempDelValue
    
    ' added this section to establish what the default value is. This value is used to restore values within ITK using bitwise checks
    ' -------------------------------------------------------------------------------------------------------------------------------
    transferTypeValue = FindID("TYPENUMBER", "ITK_TRANSFERTYPE", "TYPENAME = '" & prmTransferType & "'")
    matchTypeValue = FindID("TYPENUMBER", "ITK_MATCHTYPE", "TYPENAME = '" & prmMatchType & "'")
    deleteTypeValue = FindID("TYPENUMBER", "ITK_DELETETYPE", "TYPENAME = '" & prmDeleteType & "'")
    If deleteTypeValue = 3 Then
        tempDelValue = 2
    Else
        tempDelValue = deleteTypeValue
    End If
    defaultValue = prmEnable ' 0=disabled, 1=enabled
    defaultValue = defaultValue + 2^(matchTypeValue + 1) ' this assumes that the possible values are 0=none, 1=manual
    defaultValue = defaultValue + 2^(tempDelValue + 3) ' this assumes that the possible values are 0=no, 1=unref, 3=all( the 3 has been converted to 2 with the above if statement
    defaultValue = defaultValue + 2^(transferTypeValue + 5) ' this assumes that the possible values are 1=resync, 2=netchange, 3=netchange CRC
    ' -------------------------------------------------------------------------------------------------------------------------------

    id = CreateAction(prmProfile, FindID("TYPENUMBER", "ITK_ACTIONTYPE", "TYPENAME = 'ACTION_DATATRANSFER'"), prmEnable, defaultValue)
    If IsNull(FindID("DATATRANSFERID", "ITK_DATATRANSFER", "DATATRANSFERID = " & id)) Then
        ExecuteQuery "INSERT INTO ITK_DATATRANSFER (DATATRANSFERID, CLASSCOUPLEID, DELETETYPE, TRANSFERTYPE, MATCHTYPE) VALUES (" & _
                     id & ", " & _
                     prmClassCouple & ", " & _
                     deleteTypeValue & ", " & _
                     transferTypeValue & ", " & _
                     matchTypeValue & ")"
    Else
	ExecuteQuery "UPDATE ITK_DATATRANSFER set " & _
                     "CLASSCOUPLEID=" & prmClassCouple & ", " & _
                     "DELETETYPE=" & deleteTypeValue & ", " & _
                     "TRANSFERTYPE=" & transferTypeValue & ", " & _
                     "MATCHTYPE=" & matchTypeValue & " " & _
		     "where DATATRANSFERID=" & id
    End If
    CreateDataTransfer = id
End Function


'******************************************************************************
Function CreateClassCouple(prmCompTypeCouple, prmDescription, prmClassSrc, prmClassDst, prmDirection, prmReadBatchSize, prmWriteBatchSize)
    WScript.echo "CreateClassCouple:" & prmCompTypeCouple & "," & prmDescription & "," & prmClassSrc & "," & prmClassDst & "," & prmDirection & "," & prmReadBatchSize & "," & prmWriteBatchSize
    id = FindID("CLASSCOUPLEID", "ITK_CLASSCOUPLE", "COMPTYPECOUPLEID = " & prmCompTypeCouple & _
                " AND CLASSSRCNAME = '" & prmClassSrc & "' AND CLASSDSTNAME = '" & prmClassDst & _
                "' AND DIRECTION = " & prmDirection)
    
    If IsNull(id) Then
        id = GetNewSequenceID("ITK_CLASSCOUPLE")
        ExecuteQuery "INSERT INTO ITK_CLASSCOUPLE (CLASSCOUPLEID, COMPTYPECOUPLEID, SCRIPTID, NAME, CLASSSRCNAME, " & _
                     "CLASSDSTNAME, FILTERSRC, FILTERDST, DIRECTION, READBATCHSIZE, WRITEBATCHSIZE, FILTEREXPSRC, FILTEREXPDST) VALUES (" & _
                     id & ", " & prmCompTypeCouple & ", NULL, '" & prmDescription & "', '" & prmClassSrc & _
                     "', '" & prmClassDst & "', NULL, NULL, " & prmDirection & ", " & prmReadBatchSize & _
                     ", " & prmWriteBatchSize & ", 0, 0)"
    End If
    CreateClassCouple = id
End Function


'******************************************************************************
Function UpdateClassCouple(prmCompTypeCouple, prmClassSrc, prmClassDst, _
                           prmDirection, prmNewDescription, _
                           prmNewClassSrc, prmNewClassDst, prmNewDirection, _
                           prmNewReadBatchSize, prmNewWriteBatchSize)
    WScript.echo "UpdateClassCouple:" & prmCompTypeCouple & "," & prmClassSrc & "," & prmClassDst & "," & prmDirection & "," & prmNewDescription & "," & prmNewClassSrc & "," & prmNewClassDst & "," & prmNewDirection & "," & prmNewReadBatchSize & "," & prmNewWriteBatchSize
    id = FindID("CLASSCOUPLEID", "ITK_CLASSCOUPLE", "COMPTYPECOUPLEID = " & prmCompTypeCouple & _
                " AND CLASSSRCNAME = '" & prmClassSrc & "' AND CLASSDSTNAME = '" & prmClassDst & _
                "' AND DIRECTION = " & prmDirection)
    If Not IsNull(id) Then
        If Not IsNull(prmNewDescription) Then
            ExecuteQuery "UPDATE ITK_CLASSCOUPLE SET NAME = '" & prmNewDescription & _
                            "' WHERE CLASSCOUPLEID = " & id
        End If
        If Not IsNull(prmNewClassSrc) Then
            ExecuteQuery "UPDATE ITK_CLASSCOUPLE SET CLASSSRCNAME = '" & prmNewClassSrc & _
                            "' WHERE CLASSCOUPLEID = " & id
        End If
        If Not IsNull(prmNewClassDst) Then
            ExecuteQuery "UPDATE ITK_CLASSCOUPLE SET CLASSDSTNAME = '" & prmNewClassDst & _
                            "' WHERE CLASSCOUPLEID = " & id
        End If
        If Not IsNull(prmNewDirection) Then
            ExecuteQuery "UPDATE ITK_CLASSCOUPLE SET DIRECTION = " & prmNewDirection & _
                            " WHERE CLASSCOUPLEID = " & id
        End If
        If Not IsNull(prmNewReadBatchSize) Then
            ExecuteQuery "UPDATE ITK_CLASSCOUPLE SET READBATCHSIZE = " & prmNewReadBatchSize & _
                            " WHERE CLASSCOUPLEID = " & id
        End If
        If Not IsNull(prmNewWriteBatchSize) Then
            ExecuteQuery "UPDATE ITK_CLASSCOUPLE SET WRITEBATCHSIZE = " & prmNewWriteBatchSize & _
                            " WHERE CLASSCOUPLEID = " & id
        End If
    End If
    UpdateClassCouple = id
End Function


'******************************************************************************
Sub CreateClassCoupleDict(prmDescription, prmClassSrc, prmClassDst, prmDirection)
    WScript.echo "CreateClassCoupleDict:" & prmDescription & "," & prmClassSrc & "," & prmClassDst & "," & prmDirection
    CreateClassCoupleDictEx prmDescription, prmClassSrc, prmClassDst, prmDirection, 200, 200
End Sub


'******************************************************************************
Sub CreateClassCoupleDictEx(prmDescription, prmClassSrc, prmClassDst, prmDirection, prmReadBatchSize, prmWriteBatchSize)
    WScript.echo "CreateClassCoupleDictEx:" & prmDescription & "," & prmClassSrc & "," & prmClassDst & "," & prmDirection & "," & prmReadBatchSize & "," & prmWriteBatchSize
    Dim classCouple
    classCouple = CreateClassCouple(compTypeCouple, prmDescription, prmClassSrc, prmClassDst, prmDirection, prmReadBatchSize, prmWriteBatchSize)
    clsCplDict.Add prmDescription, classCouple
End Sub


'******************************************************************************
Sub UpdateClassCoupleDict(prmDescription, prmClassSrc, prmClassDst, prmDirection, _
                           prmNewDescription, prmNewClassSrc, prmNewClassDst, _
                           prmNewDirection, prmNewReadBatchSize, prmNewWriteBatchSize)
    WScript.echo "UpdateClassCoupleDict:" & prmDescription & "," & prmClassSrc & "," & prmClassDst & "," & prmDirection & "," & prmNewDescription & "," & prmNewClassSrc & "," & prmNewClassDst & "," & prmNewDirection & "," & prmNewReadBatchSize & "," & prmNewWriteBatchSize
    UpdateClassCoupleDictEx prmDescription, prmClassSrc, prmClassDst, prmDirection, prmNewDescription, _
                            prmNewClassSrc, prmNewClassDst, prmNewDirection, _
                            prmNewReadBatchSize, prmNewWriteBatchSize
End Sub


'******************************************************************************
Sub UpdateClassCoupleDictEx(prmDescription, prmClassSrc, prmClassDst, prmDirection, _
                            prmNewDescription, prmNewClassSrc, prmNewClassDst, _
                            prmNewDirection, prmNewReadBatchSize, prmNewWriteBatchSize)
    WScript.echo "UpdateClassCoupleDictEx:" & prmDescription & "," & prmClassSrc & "," & prmClassDst & "," & prmDirection & "," & prmNewDescription & "," & prmNewClassSrc & "," & prmNewClassDst & "," & prmNewDirection & "," & prmNewReadBatchSize & "," & prmNewWriteBatchSize
    Dim classCouple
    classCouple = UpdateClassCouple(compTypeCouple, prmClassSrc, prmClassDst, prmDirection, _
                                    prmNewDescription, prmNewClassSrc, prmNewClassDst, _
                                    prmNewDirection, prmNewReadBatchSize, prmNewWriteBatchSize)
End Sub


'******************************************************************************
Sub CreateDataTransferDict(prmDescription, prmEnable, prmDeleteType, prmTransType, prmMatchType)
    WScript.echo "CreateDataTransferDict:" & prmDescription & "," & prmEnable & "," & prmDeleteType & "," & prmTransType & "," & prmMatchType
    If Not clsCplDict.Exists(prmDescription) Then
        WScript.Echo ERROR_PREFIX & "Bad class couple description: " & prmDescription
        WScript.Quit EXIT_CODE_FAILURE
    End If
    CreateDataTransfer clsCplDict.Item(prmDescription), profile, prmEnable, prmDeleteType, prmTransType, prmMatchType
End Sub


'******************************************************************************
Function CreateProfile(prmCompInst, prmCompInstCouple, prmName, prmNote)
    WScript.echo "CreateProfile:" & prmCompInst & "," & prmCompInstCouple & "," & prmName & "," & prmNote
    rank = 1
    id = FindID("PROFILEID", "ITK_PROFILE", "NAME = '" & prmName & "'")
    If IsNull(id) Then
        ' Create ITK_PROFILE 
        id = GetNewSequenceID("ITK_PROFILE")
        ExecuteQuery "INSERT INTO ITK_PROFILE (PROFILEID, NAME, NOTE, OPTIMIZEPROCESS, ERRTEMPLATE, LOGLEVELTYPE, MODIFICATIONDATE, GLOBALCOMMIT) VALUES (" & _
                     id & ", '" & prmName & "', '" & prmNote & "', 0, 0, 1, " & sysDate & ", 0)"
        ' Create ITK_COMPINSTPROF
        ExecuteQuery "INSERT INTO ITK_COMPINSTPROF (PROFILEID, COMPINSTCOUPLEID, COMPINSTANCEID, RANK) VALUES (" & _
                     id & ", " & prmCompInstCouple & ", " & prmCompInst & ", 1)"
    Else
        If (updateInstallSupported) Then
            ExecuteQuery "DELETE [ITK_DATATRANSFER] WHERE DATATRANSFERID IN (SELECT actionid FROM ITK_ACTION WHERE PROFILEID=" & id & ")"
	    ExecuteQuery "DELETE [ITK_SCRIPTACTION] WHERE SCRIPTACTIONID IN (SELECT actionid FROM ITK_ACTION WHERE PROFILEID=" & id & ")"
            ExecuteQuery "DELETE [ITK_SERVICE] WHERE SERVICEID IN (SELECT actionid FROM ITK_ACTION WHERE PROFILEID=" & id & ")"
            ExecuteQuery "DELETE ITK_ACTION WHERE PROFILEID=" & id
        End If
    End If
    CreateProfile = id
End Function


'******************************************************************************
Sub DeprecateProfile(prmName)
    WScript.echo "DeprecateProfile:" & prmName	
	Dim deprecatedName
	deprecatedName = DEPRECATED_PREFIX & prmName
	id = FindID("PROFILEID", "ITK_PROFILE", "NAME = '" & deprecatedName & "'")
	If Not IsNull(id) Then
		WScript.Echo ERROR_PREFIX & "Deprecated Profile found: " & deprecatedName
		WScript.Quit EXIT_CODE_FAILURE 	
	End If
	
    id = FindID("PROFILEID", "ITK_PROFILE", "NAME = '" & prmName & "'")
    If IsNull(id) Then
		WScript.echo WARNING_PREFIX & "Profile not found: " & prmName
    Else
		ExecuteQuery "UPDATE ITK_PROFILE SET NAME='" & deprecatedName  & "' WHERE PROFILEID=" & id
    End If
End Sub


'******************************************************************************
Function CreateProfileGroup(prmName, prmNote)
    WScript.echo "CreateProfileGroup:" & prmName & "," & prmNote
    rank = 1
    id = FindID("PROFGROUPID", "ITK_PROFGROUP", "NAME = '" & prmName & "'")
    If IsNull(id) Then
        ' Create ITK_PROFGROUP
        id = GetNewSequenceID("ITK_PROFGROUP")
        ExecuteQuery "INSERT INTO ITK_PROFGROUP (PROFGROUPID, NAME, NOTE, EXECFLOWTYPE, MODIFICATIONDATE, GLOBALCOMMIT) VALUES (" & _
                      id & ", '" & prmName & "', '" & prmNote & "', " & _
                      FindID("TYPENUMBER", "ITK_EXECFLOWTYPE", "TYPENAME = 'EXECFLOW_SEQUENCE'") & ", " & sysDate & ", 0)"
    Else
        If (updateInstallSupported) Then
            ExecuteQuery "DELETE ITK_PROFGROUPMEMBR WHERE PROFGROUPID=" & id
        End If
    End If
    CreateProfileGroup = id
End Function


'******************************************************************************
Sub DeprecateProfileGroup(prmName)
    WScript.echo "DeprecateProfileGroup:" & prmName
	Dim deprecatedName
	deprecatedName = DEPRECATED_PREFIX & prmName	
    id = FindID("PROFGROUPID", "ITK_PROFGROUP", "NAME = '" & deprecatedName & "'")
	If Not IsNull(id) Then
		WScript.Echo ERROR_PREFIX & "Deprecated ProfileGroup found: " & deprecatedName
		WScript.Quit EXIT_CODE_FAILURE 	
	End If
	
    id = FindID("PROFGROUPID", "ITK_PROFGROUP", "NAME = '" & prmName & "'")
    If IsNull(id) Then
		WScript.echo WARNING_PREFIX & "ProfileGroup not found: " & prmName
    Else
		ExecuteQuery "UPDATE ITK_PROFGROUP SET NAME='" & deprecatedName  & "' WHERE PROFGROUPID=" & id
    End If
End Sub


'******************************************************************************
Sub CreateProfileGroupMember(prmProfileName)
    WScript.echo "CreateProfileGroupMember:" & prmProfileName
    profile = FindID("PROFILEID", "ITK_PROFILE", "NAME = '" & prmProfileName & "'")
    If IsNull(profile) Then
        WScript.Echo ERROR_PREFIX & "Profile not found: " & prmProfileName
        WScript.Quit EXIT_CODE_FAILURE 
    End If
    
    id = FindID("PROFGROUPID", "ITK_PROFGROUPMEMBR", "PROFGROUPID = " & group & " AND MEMBRPROFILEID = " & profile)
    If IsNull(id) Then
        ExecuteQuery "INSERT INTO ITK_PROFGROUPMEMBR (PROFGROUPID, MEMBRPROFGROUPID, MEMBRPROFILEID, RANK) VALUES (" & _
                     group & ", NULL, " & profile & ", " & rank & ")"
    End If
    rank = rank + 1
End Sub


'******************************************************************************
Sub CreateProfileGroupMemberEx(prmProfileGroupName)
    WScript.echo "CreateProfileGroupMemberEx:" & prmProfileGroupName
    profile = FindID("PROFGROUPID", "ITK_PROFGROUP", "NAME = '" & prmProfileGroupName & "'")
    If IsNull(profile) Then
        WScript.Echo ERROR_PREFIX & "Profile group not found: " & prmProfileGroupName
        WScript.Quit EXIT_CODE_FAILURE
    End If
    
    id = FindID("PROFGROUPID", "ITK_PROFGROUPMEMBR", "PROFGROUPID = " & group & " AND MEMBRPROFGROUPID = " & profile)
    If IsNull(id) Then
        ExecuteQuery "INSERT INTO ITK_PROFGROUPMEMBR (PROFGROUPID, MEMBRPROFGROUPID, MEMBRPROFILEID, RANK) VALUES (" & _
                     group & ", " & profile & ", NULL, " & rank & ")"
    End If
    rank = rank + 1
End Sub


'******************************************************************************
Sub CreateConnectParam(prmCompInst, prmName, prmValue)
    WScript.echo "CreateConnectParam:" & prmCompInst & "," & prmName & "," & prmValue
    id = FindID("COMPINSTANCEID", "ITK_CONNECTPARAM", "COMPINSTANCEID = " & prmCompInst & _
                " AND NAME = '" & prmName & "'")
    If IsNull(id) Then
        ExecuteQuery "INSERT INTO ITK_CONNECTPARAM (COMPINSTANCEID, NAME, VALUE) VALUES (" & _
                      prmCompInst & ", '" & prmName & "', '" & prmValue & "')"
    End If
End Sub


'******************************************************************************
Sub CreateProperty(prmCompType, prmName, prmValue, prmDescription)
    WScript.echo "CreateProperty:" & prmCompType & "," & prmName & "," & prmValue & "," & prmDescription
    id = FindID("PROPERTIESID", "ITK_PROPERTIES", "NAME = '" & prmName & "' AND " & _
                "TABLENAME = 'ITK_COMPTYPE' AND RECORDID = " & prmCompType)
    If IsNull(id) Then
        id = GetNewSequenceID("ITK_PROPERTIES")
        ExecuteQuery "INSERT INTO ITK_PROPERTIES (PROPERTIESID, TABLENAME, RECORDID, NAME, VALUE, DESCRIPTION, MODIFICATIONDATE) VALUES (" & _
                      id & ", 'ITK_COMPTYPE', " & prmCompType & ", '" & prmName & "', '" & prmValue & _
                      "', '" & prmDescription & "', " & sysDate & ")"
    Else
        'We update only the description since updating the value could be harmful
        ExecuteQuery "UPDATE ITK_PROPERTIES SET DESCRIPTION = '" & prmDescription & "' WHERE PROPERTIESID = " & id 
    End If
End Sub


'******************************************************************************
' !!!only use this function if the property value can be updated without causing harm!!!
'******************************************************************************
Sub CreateUpdateProperty(prmCompType, prmName, prmValue, prmDescription)
    WScript.echo "CreateProperty:" & prmCompType & "," & prmName & "," & prmValue & "," & prmDescription
    id = FindID("PROPERTIESID", "ITK_PROPERTIES", "NAME = '" & prmName & "' AND " & _
                "TABLENAME = 'ITK_COMPTYPE' AND RECORDID = " & prmCompType)
    If IsNull(id) Then
        id = GetNewSequenceID("ITK_PROPERTIES")
        ExecuteQuery "INSERT INTO ITK_PROPERTIES (PROPERTIESID, TABLENAME, RECORDID, NAME, VALUE, DESCRIPTION, MODIFICATIONDATE) VALUES (" & _
                      id & ", 'ITK_COMPTYPE', " & prmCompType & ", '" & prmName & "', '" & prmValue & _
                      "', '" & prmDescription & "', " & sysDate & ")"
    Else
        ExecuteQuery "UPDATE ITK_PROPERTIES SET " & _
                     "NAME = '" & prmName & "'," & _
                     "VALUE = '" & prmValue & "'," & _
                     "DESCRIPTION = '" & prmDescription & "' " & _
                     "WHERE PROPERTIESID = " & id 
    End If
End Sub


'******************************************************************************
Sub CreateCoupleProperty(prmCompTypeCouple, prmName, prmValue, prmDescription)
    WScript.echo "CreateCoupleProperty:" & prmCompTypeCouple & "," & prmName & "," & prmValue & "," & prmDescription
    id = FindID("PROPERTIESID", "ITK_PROPERTIES", "NAME = '" & prmName & "' AND " & _
                "TABLENAME = 'ITK_COMPTYPECOUPLE' AND RECORDID = " & prmCompTypeCouple)
    If IsNull(id) Then
        id = GetNewSequenceID("ITK_PROPERTIES")
        ExecuteQuery "INSERT INTO ITK_PROPERTIES (PROPERTIESID, TABLENAME, RECORDID, NAME, VALUE, DESCRIPTION, MODIFICATIONDATE) VALUES (" & _
                      id & ", 'ITK_COMPTYPECOUPLE', " & prmCompTypeCouple & ", '" & prmName & "', '" & prmValue & _
                      "', '" & prmDescription & "', " & sysDate & ")"
    Else
        'We update only the description since updating the value could be harmful
        ExecuteQuery "UPDATE ITK_PROPERTIES SET DESCRIPTION = '" & prmDescription & "' WHERE PROPERTIESID = " & id 
    End If
End Sub


'******************************************************************************
' !!!only use this function if the property value can be updated without causing harm!!!
'******************************************************************************
Sub CreateUpdateCoupleProperty(prmCompTypeCouple, prmName, prmValue, prmDescription)
    WScript.echo "CreateCoupleProperty:" & prmCompTypeCouple & "," & prmName & "," & prmValue & "," & prmDescription
    id = FindID("PROPERTIESID", "ITK_PROPERTIES", "NAME = '" & prmName & "' AND " & _
                "TABLENAME = 'ITK_COMPTYPECOUPLE' AND RECORDID = " & prmCompTypeCouple)
    If IsNull(id) Then
        id = GetNewSequenceID("ITK_PROPERTIES")
        ExecuteQuery "INSERT INTO ITK_PROPERTIES (PROPERTIESID, TABLENAME, RECORDID, NAME, VALUE, DESCRIPTION, MODIFICATIONDATE) VALUES (" & _
                      id & ", 'ITK_COMPTYPECOUPLE', " & prmCompTypeCouple & ", '" & prmName & "', '" & prmValue & _
                      "', '" & prmDescription & "', " & sysDate & ")"
    Else
        ExecuteQuery "UPDATE ITK_PROPERTIES SET " & _
                     "NAME = '" & prmName & "'," & _
                     "VALUE = '" & prmValue & "'," & _
                     "DESCRIPTION = '" & prmDescription & "' " & _
                     "WHERE PROPERTIESID = " & id
    End If
End Sub


'******************************************************************************
Function CreateComponentInstance(prmCompType, prmName)
    WScript.echo "CreateComponentInstance:" & prmCompType & "," & prmName
    id = FindID("COMPINSTANCEID", "ITK_COMPINSTANCE", "NAME = '" & prmName & "'")
    If IsNull(id) Then
        id = GetNewSequenceID("ITK_COMPINSTANCE")
        ExecuteQuery "INSERT INTO ITK_COMPINSTANCE (COMPINSTANCEID, COMPTYPEID, NAME, NOTE, MODEL, SERVER, MODIFICATIONDATE) VALUES (" & _
                      id & ", " & prmCompType & ", '" & prmName & "', NULL, 1, NULL, " & sysDate & ")"
    End If
    CreateComponentInstance = id
End Function


'******************************************************************************
Sub UpdateSystemParam(prmSysParamName, prmValue, prmDescription, prmLanguage, prmType, prmMin, prmMax, prmDefaultValue)
    WScript.echo "UpdateSystemParam:" & prmSysParamName & "," & prmValue & "," & prmDescription & "," & prmLanguage & "," & prmType & "," & prmMin & "," & prmMax & "," & prmDefaultValue
    Dim typeID
    RepairSystemSequenceNumbers
    typeID = FindID("TYPENUMBER", "ITK_SYSPARAMTYPE", "TYPENAME = '" & prmSysParamName & "'")
    If IsNull(typeID) Then
        typeID = GetNewSequenceID("ITK_SYSPARAM")
        ExecuteQuery "INSERT INTO ITK_SYSPARAMTYPE (TYPENUMBER, TYPENAME, TYPEDESCRIPTION, TYPELANGUAGE) VALUES (" & _
                      typeID & ", '" & prmSysParamName & "', '" & prmDescription & "', '" & prmLanguage & "')"
        ExecuteQuery "INSERT INTO ITK_SYSPARAM (SYSPARAMID, PARAMGROUP, PARAMTYPE, PARAMNUMBER, PARAMVALUE, PARAMMINVALUE, PARAMMAXVALUE, DEFAULTVALUE) VALUES (" & _
                      typeID & ", 0, " & prmType & ", " & typeID & ", '" & prmValue & "', '" & prmMin & "', '" & prmMax & "', '" & prmDefaultValue & "')"
    Else
        ExecuteQuery "UPDATE ITK_SYSPARAM SET PARAMVALUE = '" & prmValue & "' WHERE PARAMNUMBER = " & typeID 
        ExecuteQuery "UPDATE ITK_SYSPARAM SET DEFAULTVALUE = '" & prmDefaultValue & "' WHERE PARAMNUMBER = " & typeID 
    End If
End Sub


'******************************************************************************
'f Repairs sequence numbers if they are corrupted
'******************************************************************************
Sub RepairSystemSequenceNumbers
    WScript.echo "RepairSystemSequenceNumbers"
    ExecuteQuery "UPDATE ITK_SEQUENCE SET CURVAL = (SELECT MAX(TYPENUMBER) FROM ITK_SYSPARAMTYPE) WHERE TABLENAME = 'ITK_SYSPARAM'"
End Sub


'******************************************************************************
'f Open a connection using ADO
'******************************************************************************
Sub OpenConnection()
    WScript.echo "OpenConnection"
    On Error Resume Next
    Dim dbType, connectString

    If WScript.Arguments.Count < 2 Then
        WScript.echo ERROR_PREFIX & "Wrong number of arguments."
        WScript.echo "Usage: " & WScript.ScriptName & " <ORACLE|MSSQL> <connectionstring>"
        WScript.echo ""
        WScript.echo "Arguments passed:" & WScript.Arguments.Count
        Dim argument
        For each argument in WScript.arguments
            WScript.echo argument
        Next
        WScript.Quit EXIT_CODE_FAILURE
    End If
 
    dbType = WScript.Arguments(0)
    connectString = WScript.Arguments(1)
    updateInstallSupported = (WScript.Arguments(2) = 1)
    WScript.echo "updateInstallSupported=" & updateInstallSupported
    
    If (dbType=OLEDB_DBTYPE_SQLSERVER) Then
        WScript.echo "SQLServer"
        sysDate = "GetDate()"   
    Else
        If (dbType=OLEDB_DBTYPE_ORACLE) Then
           WScript.echo "ORACLE"
           sysDate = "SYSDATE"     
        Else
           WScript.echo ERROR_PREFIX & "INVALID DATABASE TYPE; dbType=" & dbType
           WScript.Quit EXIT_CODE_FAILURE
        End IF
    End IF

    Set connP = CreateObject("ADODB.Connection")
    connP.ConnectionString = connectString
    connP.Open()
    If Err.Number <> 0 Then
        WScript.Echo ERROR_PREFIX & Err.Description
        WScript.Quit EXIT_CODE_FAILURE
    End If    

    Set clsCplDict = CreateObject("Scripting.Dictionary")
    clsCplDict.CompareMode = 1      ' Text compare
End Sub


'******************************************************************************
'f Close a connection
'******************************************************************************
Sub CloseConnection()
    WScript.echo "CloseConnection"
    On Error Resume Next
    connP.Close()
    If Err.Number <> 0 Then
        WScript.Echo ERROR_PREFIX & Err.Description
        WScript.Quit EXIT_CODE_FAILURE
    End If
End Sub


'******************************************************************************
Function ExecuteQuery(prmQuery)
    WScript.echo "ExecuteQuery:" & prmQuery
    On Error Resume Next
    Dim status

    Do 
        status = vbOk
        Set ExecuteQuery = connP.Execute(prmQuery)
        If Err.Number <> 0 Then
            WScript.Echo ERROR_PREFIX & Err.Description & vbCr & vbCr & "Query: " & prmQuery
            WScript.Quit EXIT_CODE_FAILURE
        End If
    Loop While status = vbRetry
End Function


'******************************************************************************
Function CreateComponentType(prmName, prmDesc, prmAdapter)
    WScript.echo "CreateComponentType:" & prmName & "," & prmDesc & "," & prmAdapter
    id = FindID("COMPTYPEID", "ITK_COMPTYPE", "NAME = '" & prmName & "'")
    If IsNull(id) Then
        id = GetNewSequenceID("ITK_COMPTYPE")
        ExecuteQuery "INSERT INTO ITK_COMPTYPE (COMPTYPEID, NAME, NOTE, ADAPTERNAME, MODIFICATIONDATE) VALUES (" & _
                     id & ", '" & prmName & "', '" & prmDesc & "', '" & prmAdapter & "', " & sysDate & ")"
    End If
    CreateComponentType = id
End Function


'******************************************************************************
Function CreateComponentTypeCouple(prmCompType1, prmCompType2)
    WScript.echo "CreateComponentTypeCouple:" & prmCompType1 & "," & prmCompType2
    id = FindID("COMPTYPECOUPLEID", "ITK_COMPTYPECOUPLE", _
                "COMPTYPE1ID = " & prmCompType1 & " AND COMPTYPE2ID = " & prmCompType2)
    If IsNull(id) Then
        id = GetNewSequenceID("ITK_COMPTYPECOUPLE")
        ExecuteQuery "INSERT INTO ITK_COMPTYPECOUPLE (COMPTYPECOUPLEID, COMPTYPE1ID, COMPTYPE2ID, NOTE, MODIFICATIONDATE) VALUES (" & _
                     id & ", " & prmCompType1 & ", " & prmCompType2 & ", NULL, " & sysDate & ")"
    End If
    CreateComponentTypeCouple = id
End Function


'******************************************************************************
Function CreateComponentInstanceCouple(prmCompTypeCouple, prmCompInst1, prmCompInst2)
    WScript.echo "CreateComponentInstanceCouple:" & prmCompTypeCouple & "," & prmCompInst1 & "," & prmCompInst2
    id = FindID("COMPINSTCOUPLEID", "ITK_COMPINSTCOUPLE", "COMPTYPECOUPLEID = " & prmCompTypeCouple & _
                " AND COMPINSTANCE1ID = " & prmCompInst1 & " AND COMPINSTANCE2ID = " & prmCompInst2)
    If IsNull(id) Then
        id = GetNewSequenceID("ITK_COMPINSTCOUPLE")
        ExecuteQuery "INSERT INTO ITK_COMPINSTCOUPLE (COMPINSTCOUPLEID, COMPINSTANCE1ID, COMPINSTANCE2ID, COMPTYPECOUPLEID) VALUES (" & _
                     id & ", " & prmCompInst1 & ", " & prmCompInst2 & ", " & prmCompTypeCouple & ")"
    End If
    CreateComponentInstanceCouple = id
End Function


'******************************************************************************
Function FindID(prmField, prmTable, prmCondition)
    WScript.echo "FindID:" & prmField & "," & prmTable & "," & prmCondition
    Set rsP = ExecuteQuery("SELECT " & prmField & " FROM " & prmTable & " WHERE " & prmCondition)
    If rsP.EOF Then
        FindID = Null
    Else
        FindID = CLng(rsP.Fields(prmField))
    End If
    WScript.echo "FindID;ID=" & FindID
End Function


'******************************************************************************
Function GetNewSequenceID(prmTableName)
    WScript.echo "GetNewSequenceID:" & prmTableName
    Dim count, curVal
    ' Look for existing ITK_SEQUENCE first
    Set rsP = ExecuteQuery("SELECT COUNT(TABLENAME) FROM ITK_SEQUENCE WHERE TABLENAME = '" & prmTableName & "'" )
    If CInt(rsP.Fields(0).Value) = 0 Then
        ' Create the ITK sequence
        ExecuteQuery "INSERT INTO ITK_SEQUENCE (TABLENAME, CURVAL) VALUES ('" & prmTableName & "', 1)"
        GetNewSequenceID = 1
        Exit Function
    End If
    ' Increment current value
    ExecuteQuery("UPDATE ITK_SEQUENCE SET CURVAL = CURVAL + 1 WHERE TABLENAME = '" & prmTableName & "'")
    ' Read next sequence value
    Set rsP = ExecuteQuery("SELECT CURVAL FROM ITK_SEQUENCE WHERE TABLENAME = '" & prmTableName & "'")
    GetNewSequenceID = rsP.Fields("CURVAL")
    WScript.echo "GetNewSequenceID;ID=" & GetNewSequenceID
End Function
