REM *****************************
REM CONFIGURATION SECTION
REM *****************************

SET ItkPath=C:\Program Files (x86)\Infor\Planning Integration\ITKServerLdr.exe

SET UpdateInstall=1
SET Provider=2
SET Database=InforITK_LN_PLANNER
SET Server=carongrusseau01
SET User=sa
SET psw=xxx

REM Remove SSPI to connect using SQL Server authentication:
SET Security=

SET connectString="Provider=SQLOLEDB;Integrated Security=%Security%;Persist Security Info=True;Initial Catalog=%Database%;Password=%psw%;User ID=%User%;Data Source=%Server%"

REM ***********************************************
REM Create integration profiles in the ITK database
REM ***********************************************

COPY .\DB-BaanERP-Planner.vbs + .\Functions.vbs .\PopulateDB.vbs
cscript PopulateDB.vbs %Provider% %connectString% %UpdateInstall%
DEL PopulateDB.vbs

PAUSE