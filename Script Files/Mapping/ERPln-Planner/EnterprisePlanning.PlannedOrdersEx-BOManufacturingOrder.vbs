'****************************************************************************
' %name:           EnterprisePlanning.PlannedOrders-BOManufacturingOrder.vbs %
' %version:        1 %
' %created_by:     groussea %
' %derived_by:     groussea %
' %date_modified:  2016/06/15 %
'****************************************************************************
' Mapping: PlannedOrder(line)       -> BOManufacturingOrder
'          CapacityUsed             -> BOBomStep, BOOperationStep, BOSpecializedOperation, BOPotentialResource
'          PlannedInventoryMovement -> BOSOStepDemandTransaction, BOSOStepSupplyTransaction, BOSpecializedBOMItem
'****************************************************************************
'G-- General logic:
'   - The MO will be attached to the correct routing only if the routing is part
'     of the MO item's process list (unless the business rules will complain)
'*****************************************************************************
Option Explicit

Dim gBOOperationStepP
Dim gBOBOMStepP
Dim gBOSOStepDemandTransactionP
Dim gBOSOStepSupplyTransactionP
Dim gBOSpecializedBOMItemP

Dim gMoPrefix
Dim gMoItemP

Dim gBomStepDict
Dim gRoutingDict

Dim gCalculationMethod

Dim gEPActualScenarioStartDate
Dim gInitialStep

Dim gParallelOperations

Dim gEPFromStatusFilter
Dim gEPPrefix


'***********************************************************
' Private members
'***********************************************************
Class BOMStepClass
    ' Public members
    Public bomStepP_
    Public nextOperation_
    Public prevOperation_
End Class

'****************************************************************************
's Called once at the beginning of the transfer, before getting any data from the source
'
'p prmClassSrcP [ITKClass]  Object that represents the source class
'p prmClassDstP [ITKClass]  Object that represents the destination class
'****************************************************************************
Public Sub OnStartDataTransfer(prmClassSrcP, prmClassDstP)

    ITKValidateConfiguration prmClassSrcP, "EnterprisePlanning.PlannedOrdersEx", prmClassDstP, "BOManufacturingOrder", _
                             UE_MATCH_MANUAL, -1, UE_TRANSFER_RESYNC Or UE_TRANSFER_CRC
    E2PStartDataTransfer()

    Set gBOOperationStepP                = ITKGetClass(osConnectionP, "BOOperationStep")
    Set gBOSpecializedBOMItemP           = ITKGetClass(osConnectionP, "BOSpecializedBOMItem")
    Set gBOSOStepDemandTransactionP      = ITKGetClass(osConnectionP, "BOSOStepDemandTransaction")
    Set gBOSOStepSupplyTransactionP      = ITKGetClass(osConnectionP, "BOSOStepSupplyTransaction")
    Set gBOBOMStepP                      = ITKGetClass(osConnectionP, "BOBOMStep")

    BCCreateDict gBomStepDict
    BCCreateDict gRoutingDict

    erpBAANScenariosPrimaryP("Scenario") = ERPGetEPParameters().actualScenario
    Dim epScenarioP
    Set epScenarioP = erpBAANScenariosP.FindObject(erpBAANScenariosPrimaryP)
    gEPActualScenarioStartDate = epScenarioP.planStartDate
    
    Dim sfcParamsP
    Set sfcParamsP = erpBAANSFCParametersP.FindObject("SequenceNumber=1")

    'Get SFC parameters to renumber the operations with the step size specified in BaaN.
    If Not sfcParamsP Is Nothing Then
        gInitialStep = sfcParamsP.InitialValueForRenumberingOperations
    Else
        ITKRaiseTechFatal erpMsgFileP.FormatMessage(ERP_MSG_SFCPARAMETERS_NOT_FOUND, erpCompany)
    End If
    
    gEPFromStatusFilter                  = ITKReadProperty(Null, "EPFromStatusFilter", "10", Null, Null, Null)
    gEPPrefix                            = ITKReadProperty(Null, "EPPrefix", "EP", Null, Null, Null)    
    
End Sub

'****************************************************************************
's Called once at the end of the transfer.
'
'p prmClassSrcP [ITKClass]  Object that represents the source class
'p prmClassDstP [ITKClass]  Object that represents the destination class
'****************************************************************************
Public Sub OnTerminateDataTransfer(prmClassSrcP, prmClassDstP)
    Set gBOOperationStepP           = Nothing
    Set gBOSpecializedBOMItemP      = Nothing
    Set gBOSOStepDemandTransactionP = Nothing
    Set gBOSOStepSupplyTransactionP = Nothing
    Set gBOBOMStepP                 = Nothing
    Set gBomStepDict                = Nothing
    Set gRoutingDict                = Nothing
    Set gMoItemP                    = Nothing

    E2PTerminateDataTransfer()
End Sub

'****************************************************************************
'f Called to allow the scripter to specify the source filter
'
'p prmClassP        [ITKClass] Source class
'p prmCurrentFilter [String]   The current filter in ITKManager
'
'r [String] The new computed filter
'****************************************************************************
Public Function OnGetSourceFilter(prmClassP, prmCurrentFilter)
    OnGetSourceFilter = ERPGetPlannerFilter("ListPlannedManufacturingOrdersForPlanner", "Scenario=" & BCAddQuotes(ERPGetEPParameters().ActualScenario), False)
    OnGetSourceFilter = OnGetSourceFilter & ",FromOrderStatusE2P=" & gEPFromStatusFilter
End Function

'****************************************************************************
'f Called to allow the scripter to specify the target filter
'
'p prmClassP         [ITKClass] Source class
'p prmCurrentFilter  [String]   The current filter in ITKManager
'
'r [String] The new computed filter
'****************************************************************************
Function OnGetTargetFilter(prmClassP, prmCurrentFilter)
    If Not e2pSiteAsEU Then
        OnGetTargetFilter = "warehouseItemP.warehouseP.siteP = " & PLGetCurrentSite().id
    End If

    OnGetTargetFilter = OnGetTargetFilter & "code % " & BCAddQuotes(gEPPrefix & ".*")

End Function

'****************************************************************************
'f Called to allow the scripter to find the object in the
'  destination application. So the object will updated instead
'  of inserted.
'
'p prmSrcP   [ITKObject]  Object that represents the current record from the source
'
'r [ITKObject] Object found in the destination or Nothing if the object must be inserted
'****************************************************************************
Function OnManualMatch(prmSrcP)
    OnManualMatch = UE_SKIP_RECORD

    ERPGetBaanItem().SetPlanningItem()
    Set gMoItemP = E2PFindItem(prmSrcP, prmSrcP.PlanItem, Empty, Empty, Null)

    gParallelOperations = True

    Dim code
    code = gEPPrefix & osgensep & Trim(prmSrcP.PlannedOrder)    

    If Not gMoItemP Is Nothing Then
        plBOManufacturingOrderPrimaryP("warehouseItemP") = gMoItemP
        plBOManufacturingOrderPrimaryP("code") = code
        ' cls is set in OnStartDataTransfer
        Set OnManualMatch = plBOManufacturingOrderP.FindObject(plBOManufacturingOrderPrimaryP)
    End If
End Function

'****************************************************************************
'f Called once for each record from the source,
'  after the automatic copy performed by the ITK Server
'
'p prmSrcP          [ITKObject]  Object that represents the current
'p prmDstP          [ITKObject]  Object that represents the record
'                                that will be saved in the destination
'p prmIsNewObject   [Boolean]    True if the destination object does not
'
'r UE_OK:           Ok. Continue to the next step
'  UE_SKIP_RECORD:  Skip the record. It will not be saved in the destination
'****************************************************************************
Public Function OnAfterCopy(prmSrcP, prmDstP, prmIsNewObject)

    OnAfterCopy = UE_SKIP_RECORD

    ' BOManufacturingOrder header
    If prmIsNewObject Then
        ' Set no-update fields
        prmDstP.code = plBOManufacturingOrderPrimaryP("code")
        prmDstP.FK_warehouseItemP = gMoItemP
    Else
        prmDstP.batchUpdateIfExists = True
        ' Delete all steps and reimport.
        prmDstP.SOStepPV.Clear()
    End If
    
    ' Always reset the planned dates for the released orders, because we regenerate all the SOSteps
    prmDstP.plannedStartDate    = Null
    prmDstP.plannedEndDate      = Null
    prmDstP.propagationType     = PT_NoPropagation
    
    Select Case prmSrcP.OrderStatus
        Case BAAN_CP_RRP_OSTA_PLANNED
            'prmDstP.Status             =  SOS_Planned
            prmDstP.EPStatus      =  SOS_Planned
            'prmDstP.EPStatusText  =  "Planned"
        Case BAAN_CP_RRP_OSTA_FIRM_PLANNED
            'prmDstP.Status             =  SOS_FirmPlanned
            prmDstP.EPStatus      =  SOS_FirmPlanned
            'prmDstP.EPStatusText  =  "Firm Planned"
        Case BAAN_CP_RRP_OSTA_CONFIRMED
            'prmDstP.Status             =  SOS_Confirmed
            prmDstP.EPStatus      =  SOS_Confirmed
            'prmDstP.EPStatusText  =  "Confirmed"
    End Select

    'The right status will be set back after the REQ engine.    
    prmDstP.Status          =  SOS_Released
    
    ' The followings may be adjusted later, depending on production planning

    prmDstP.quantity            = prmSrcP.Quantity
    prmDstP.dueDate             = prmSrcP.Date

    ' Must set this date to maximum.  This will force the business rules
    ' to recalculate the MO dueStartDate, dueFinishDate, and dueDate.
    prmDstP.dueFinishDate       = #2040-01-01#
    prmDstP.plannedFinishDate   = Null

    If plVersion >= "2.4.0" Then
        ' Set planner.  Don't bother if not found.
        prmDstP.FK_actualPlannerP = PLFindWarehouseItemGroup(E2PBuildWIGCode(e2pEmployeePrefix, prmSrcP.Planner), CT_BOPlanner, True)
    End If

    ' Save the MO
    prmDstP.SaveObject()

    gParallelOperations = False

    ' Create operations
    CreateOperations prmSrcP, prmDstP, gBomStepDict
    ' Create transactions from estimated material
    CreateTransactions prmSrcP, prmDstP, gBomStepDict, prmSrcP.PlannedInventoryMovementsByItemEx
    
    ' Create pred-succ relations
    Dim ok
    ok = CreateRelations(prmSrcP, prmDstP, gBomStepDict)

    If ok Then
        ' Attach the routing the the manufacturing order.
        Dim sourcingProcessP
        Set sourcingProcessP = FindRouting(prmSrcP, prmSrcP.PlanItem, prmSrcP.Cluster, prmSrcP.Routing)
        If Not sourcingProcessP Is Nothing Then
            ' We have to be careful, because if the routing is not in the MO's warehouse item process
            ' list, the business rules will complain that the routing is not valid for the manufacturing order.
            ' In this case we just leave the routing blank.
            If IsInProcessList(gMoItemP, sourcingProcessP) Then
                prmDstP.FK_sourcingProcessP = sourcingProcessP
            Else
                ' Unlink the routing
                'KV2EP.so
                'ITKLogWarning UE_TYPE_UNKNOWN, prmSrcP, Null, e2pMsgFileP.FormatMessage(E2P_MSG_CANT_ATTACH_ROUTING, PLGetDefaultSiteCode(E2PGetEU(prmSrcP.EnterpriseUnit)), sourcingProcessP.code, gMoItemP.uniqueCode)
                'KV2EP.eo
                prmDstP.sourcingProcessP = 0
            End If
        End If

        ITKLogOk ITKGetTrnType(prmIsNewObject), prmSrcP, prmDstP, ""
    Else
        ' Mo will be deleted by the Planner BR
        ITKLogError UE_TYPE_DELETE, prmSrcP, prmDstP, osMsgFileP.FormatMessage(PL_MSG_MO_INVALID_QTY, PLGetDefaultSite(null))
    End If
End Function

'****************************************************************************
's Create specialized operations, potential resources, operation steps from Baan SFC
'  production planning records
'
'p prmSrcP              [ITKObject]     Record of the SFC order on which these operations are related
'p prmDstP              [ITKObject]     Planner MO
'p prmBomStepDict       [Dictionary]    Dictionary (will be filled with the BOM steps of the MO)
'****************************************************************************
Public Sub CreateOperations(prmSrcP, prmDstP, prmBomStepDict)

    Dim firstOperationP, lastOperationP, bomStepP, qtyToSupply
    Dim operationP, routingOpP

    prmBomStepDict.RemoveAll()

    Set firstOperationP = Nothing
    Set lastOperationP  = Nothing
    Set bomStepP        = Nothing


    ' Loop for all operations
    For Each operationP in prmSrcP.PlannedCapacityUseEx

        If firstOperationP Is Nothing Then
            ' Search for the first operation if not already found
            Set firstOperationP = operationP
        End If
        
        If operationP.NextOperation = 0 Then
            ' Search for the last operation
            Set lastOperationP = operationP
        End If

        ' Calculate the quantity to supply
        qtyToSupply = prmSrcP.Quantity

        ' ***********************
        ' BOSpecializedOperation
        ' ***********************
        Dim calculatedCycleTime
        
        'Convert from hours to minutes...
        calculatedCycleTime = operationP.ProductionTime * 60
        
        'Remove the average setup time from the production time and make sure the result is not negative althought it should never happen
        calculatedCycleTime = BCMax(calculatedCycleTime - operationP.AverageSetupTime, 0)

        If operationP.FixedDuration = BAAN_TC_YESNO_NO Then
            calculatedCycleTime = calculatedCycleTime / prmSrcP.Quantity
        End If

        Dim MachineOccupation, ManOccupation, actualMachineOccupation, CalculatedMachineOccupation
        Dim taskDescription
        
        MachineOccupation   = 1
        ManOccupation       = 1
        taskDescription     = ""
        
        
        'Pick the first one...
        For Each routingOpP in operationP.RoutingOperationsEx
            
            taskDescription = routingOpP.taskDescription
        
            If routingOpP.ManOccupationForProduction > 0 Then
                ManOccupation       = routingOpP.ManOccupationForProduction
            'Else
            '    application.print operationP.operation & " : ManOccupationForProduction is 0"    
            End If
            
            If routingOpP.MachineOccupation > 0 Then
                MachineOccupation   = routingOpP.MachineOccupation
            ElseIf routingOpP.ManOccupationForProduction > 0 Then
                MachineOccupation = routingOpP.ManOccupationForProduction
                'application.print operationP.operation & " : MachineOccupation is 0"    
            End If
                
            Exit For
        Next

'       2010/05/11 Machine Occupation is now mapped from routingOpP.MachineOccupation....
'       Lets validate that Machine Occupation from routing = calculated Machine Occupation
'       We will not look at values lower than 15 minutes to avoid rounding issues...
        If operationP.cycleTime > 15 And calculatedCycleTime > 15 Then
            CalculatedMachineOccupation = calculatedCycleTime / operationP.cycleTime
            
            If Abs(MachineOccupation - CalculatedMachineOccupation) > 1/10 Then
                
                '2011/04/18 The machine occupation for the phantom routings is not available. In this case, use the calculated machine occupation.
                If operationP.operation >= gInitialStep Then
                    If MachineOccupation = 1 And CalculatedMachineOccupation > 1/10 Then
                        MachineOccupation = CalculatedMachineOccupation
                    End If    
                Else
                    ITKLogWarning UE_TYPE_UNKNOWN, RRMFormatBaanKeyFields(prmSrcP), RRMFormatPlannerKeyFields(prmDstP), "Capacity used (cprrp2500m000) does not correspond with routing data (tirou1502m000) for operation " & operationP.Operation & "."
                End If
            End If
        End If

        '2010/05/11 Make sure to divide by the real MachineOccupation
        If MachineOccupation > 0 Then
            calculatedCycleTime = calculatedCycleTime / MachineOccupation
        Else
            calculatedCycleTime = operationP.cycleTime
        End If

        '***************************************************
        'Create parallel operations if MachineOccupation > 1
        '***************************************************
        Dim TotalOpNb, ParallelOpNo, BuildParallelOp, LastUnitOccupation
        
        TotalOpNb           = 1
        BuildParallelOp     = False

        If actualMachineOccupation > 101/100 And gParallelOperations Then

            '2010/02/02 Make sure the resource exists before proceeding with parallel operations...
            Dim resourceP
            Set resourceP = PLFindProductionResource(operationP.Resource, PLGetDefaultSite(null))
            
            If Not resourceP is Nothing Then
                
                BuildParallelOp     = True
            
                'We need one operation to start and one operation to close the parallel group
                TotalOpNb           = Int(actualMachineOccupation) + 2
                
                'LastUnitOccupation will contain the last unit occupation
                If actualMachineOccupation - Int(actualMachineOccupation) > 0 Then
                    TotalOpNb = TotalOpNb + 1
                    LastUnitOccupation = actualMachineOccupation - Int(actualMachineOccupation)
                Else
                    LastUnitOccupation = 1
                End If
            End If
            
            Set resourceP = Nothing
        End If

        For ParallelOpNo = 1 to TotalOpNb

            Dim potRscP
            Set potRscP = Nothing
    
            Dim Suffix, BuildOperation, NextOperation
            If BuildParallelOp Then
                If ParallelOpNo = 1 Then
                    'This is the start operation, create a BOM step with no suffix
                    BuildOperation  = False
                    Suffix          = ""
                ElseIf ParallelOpNo = TotalOpNb Then
                    'This is the closing operation, create a BOM step with -999 suffix
                    BuildOperation  = False
                    Suffix          = "-999"
                Else
                    BuildOperation  = True
                    Suffix          = "-" & BCPadWithZeros(ParallelOpNo-1, 3)
                End If
            Else
                BuildOperation      = True
                Suffix              = ""
            End If          


            Dim calculatedCycleTime_
            If BuildParallelOp And (ParallelOpNo + 1) = TotalOpNb And LastUnitOccupation <> 1 Then
                'This is the last parallel operation... it could be fraction
                 calculatedCycleTime_ = calculatedCycleTime * LastUnitOccupation
            Else
                 calculatedCycleTime_ = calculatedCycleTime
            End If
                    
                    
            If BuildOperation Then
    
                Set potRscP = E2PAddOperation(operationP, Nothing, prmDstP.code & osGenSep & BCPadWithZeros(operationP.Operation, 3) & Suffix, _
                                              Null, Null, operationP.FixedDuration, taskDescription, BAAN_TC_YESNO_NO, _
                                              operationP.TransferLotSize, operationP.Timeunit, operationP.TransferDelay, operationP.YieldPercentage, _
                                              operationP.ScrapQuantity, calculatedCycleTime_, 1, _
                                              operationP.AverageSetupTime, operationP.Resource, null, 0, 0)
'                                              
'                                Public Function E2PAddOperation(prmSrcP, prmProcessStepP, prmCode, prmEffectiveDate, prmExpiryDate, _
'                                prmIsFixedDuration, prmDescription, prmIsTransferBatch, prmTransferLot, _
'                                prmTimeUnit, prmTransferDelay, prmYieldPercentage, prmRejectConstant, _
'                                prmOperDurationMinutes, prmQuantity, prmFixedSetupTimeMinutes, prmWorkCenter, _
'                                prmSiteCode, prmMoveTime, prmQueueTime)

                                              
            End If

            If operationP.startTime > operationP.FinishTime Or operationP.startTime <= gEPActualScenarioStartDate Then

                Dim TimeToPlan_
                If operationP.FixedDuration = BAAN_TC_YESNO_NO Then
                    TimeToPlan_ = (operationP.AverageSetupTime * 60) + (calculatedCycleTime_ * 60 * qtyToSupply)
                Else
                    TimeToPlan_ = (operationP.AverageSetupTime * 60) + (calculatedCycleTime_ * 60)
                End If
                
                operationP.startTime = DateAdd("s", -1, operationP.FinishTime)
                'operationP.startTime = CalculateDueStartDate(operationP.FinishTime, TimeToPlan_, operationP.Resource, PLGetDefaultSite(null))
                ITKLogError UE_TYPE_DELETE, prmSrcP, Null,  "Planned order '" & Trim(prmSrcP.PlannedOrder) & "' Start time of operation '" & operationP.Operation & " is after the finish time." & VbCr & _
                                                            "Start time will be calculated base on production time, calendar periods and workcenter efficiency."
            End If

            ' MOperationStep or BOBOMStep
            Dim stepDueDate
            stepDueDate = operationP.FinishTime
    
            If potRscP Is Nothing Then
                ' If operationP is completed or operationP not valid, create a bomStepP
                Set bomStepP = gBOBOMStepP.NewObject()
            Else
                Set bomStepP = gBOOperationStepP.NewObject()
                ' To avoid getting an error message in the style: start date before end date...
                bomStepP.dueStartDate = operationP.StartTime
                bomStepP.FK_operationP = potRscP.FK_operationP
            End If

            If Suffix = "" Then
                'No parallel operation or starting step of parallel steps
                'bomStepP.rrcmERPCapacityUsed = operationP.ProductionTime * HOUR_TO_SECOND
            End If
    
            'bomStepP.code                   = BCPadWithZeros(operationP.Operation, 3)
            bomStepP.code                   = BCPadWithZeros(operationP.Operation, 3) & Suffix  'KV2EP.n
            bomStepP.FK_plannedSupplyOrderP = prmDstP
            bomStepP.qtyToSupply            = qtyToSupply
            
            If BuildParallelOp And (ParallelOpNo + 1) = TotalOpNb And LastUnitOccupation <> 1 Then
                Dim TimeToPlan
                If operationP.FixedDuration = BAAN_TC_YESNO_NO Then
                    TimeToPlan = (operationP.AverageSetupTime * 60) + (calculatedCycleTime_ * 60 * qtyToSupply)
                Else
                    TimeToPlan = (operationP.AverageSetupTime * 60) + (calculatedCycleTime_ * 60)
                End If

                bomStepP.dueDate = CalculateDueFinishDate(bomStepP.dueStartDate, TimeToPlan, operationP.Resource, PLGetDefaultSite(null))
            Else
                bomStepP.dueDate = stepDueDate
            End If

            bomStepP.SaveObject()
           ' Add the step in the dictionary

            If BuildParallelOp Then
                If ParallelOpNo = 1 Then
                    'The starting must be a BOM step with due date = due start date of operation in ERP
                    bomStepP.dueDate = BCMin(operationP.StartTime, stepDueDate)
                    AddBOMStep bomStepP, Empty, prmBomStepDict
                ElseIf ParallelOpNo = TotalOpNb Then
                    AddBOMStep bomStepP, operationP.NextOperation, prmBomStepDict
                Else
                    AddBOMStepEx bomStepP, BCPadWithZeros(operationP.operation, 3) & "-999", operationP.operation, prmBomStepDict
                End If
            Else
                AddBOMStep bomStepP, operationP.NextOperation, prmBomStepDict
            End If    
        Next    
    Next

    ' End of operations

    If bomStepP Is Nothing Then
        ' If there are no sosteps on the manufacturing order (means no operations in Baan, or all completed),
        ' we have to create a dummy BOMStep.
        ' BOBomStep
        Set bomStepP                    = gBOBOMStepP.NewObject()
        bomStepP.code                   = "000"
        bomStepP.FK_plannedSupplyOrderP = prmDstP
        bomStepP.qtyToSupply            = prmDstP.Quantity
        bomStepP.dueDate                = prmDstP.dueDate
        bomStepP.SaveObject()
        ' Add the step in the dictionary
        AddBOMStep bomStepP, 0, prmBomStepDict
    End If
End Sub

'****************************************************************************
's Add a BOMStep to the dictionay
'****************************************************************************
Private Sub AddBOMStep(prmBomStepP, prmNextOperation, prmBomStepDict)
    Dim bomStepClsP
    Set bomStepClsP = New BOMStepClass
    Set bomStepClsP.bomStepP_ = prmBomStepP
    bomStepClsP.nextOperation_ = prmNextOperation
    prmBomStepDict.Add prmBomStepP.code, bomStepClsP
End Sub

'****************************************************************************
's Create demand and supply transactions from Baan estimated material costs
'
'p prmSrcP        [ITKObject]     Record of the Baan MO on which these transactions are related
'p prmDstP        [ITKObject]     Planner manufacturing order
'p prmBomStepDict [Dictionary]    Dictionary of BOM steps of the MO
'p prmMaterialsPV [ITKVector]     List of estimated materials
'****************************************************************************
Private Sub CreateTransactions(prmSrcP, prmDstP, prmBomStepDict, prmMaterialsPV)

    Dim materialCostP

    For Each materialCostP In prmMaterialsPV
        ' Find the associated planner item of the transactionP
        Dim subItemP
	    
	    '2008/12/05 Use the enterprise unit of the planned inventory movement can be different than the produced item
	    'Example: Item 328-065-00 is produced in EU BR-MS1 but material comes from EU BR-OU1. All within cluster B01. 
	    'Lets try to consume the item in a different site in Planner and see what it does...
	    'Set subItemP = E2PFindItem(prmSrcP, materialCostP.PlanItem, materialCostP.DefaultWarehouse, Empty, Null)                                         
	    Set subItemP = E2PFindItem(prmSrcP, materialCostP.PlanItem, materialCostP.DefaultWarehouse, null, Null)                                         

        ' BOSOStepDemandTransaction or BOSOStepSupplyTransaction
        If Not subItemP Is Nothing Then
            ' SubItem is valid: now find the corresponding associated BOMStep
            Dim bomStepP
            Set bomStepP = FindBOMStep(materialCostP.Operation, prmBomStepDict)
            ' Normally a BOM step should be found!  If not, something is wrong in CreateOperations!
            Dim bomStepDueDate
            If bomStepP.cls = CT_BOOperationStep Then
                bomStepDueDate = bomStepP.dueStartDate
            Else
                bomStepDueDate = bomStepP.dueDate
            End If

            CreateSOStepTransaction subItemP, bomStepP, materialCostP.PositionNumber, materialCostP.TransactionType, materialCostP.quantity, bomStepDueDate, prmDstP
                                    
        End If
    Next
End Sub

'****************************************************************************
's Create specialized operations, potential resources, operation steps from Baan SFC
'  production planning records
'
'p prmSrcP        [ITKObject]     Baan production order
'p prmDstP         [ITKObject]     Planner MO
'p prmBomStepDict [Dictionary]    Dictionary (will be filled with the BOM steps of the MO)
'****************************************************************************
Public Function CreateRelations(prmSrcP, prmDstP, prmBomStepDict)
    Dim bomStepClsP, supplyingStepP

    Set supplyingStepP = Nothing

    For Each bomStepClsP In prmBomStepDict.Items
        ' At the same time, update the pred/succ relations

'KV2EP.sn
        If bomStepClsP.prevOperation_ > 0 Then
            Dim prevStepP
            Set prevStepP = FindBOMStep(bomStepClsP.prevOperation_, prmBomStepDict)
                prevStepP.soStepSuccPV.Append bomStepClsP.bomStepP_
        End If    

        If Not IsEmpty(bomStepClsP.nextOperation_) Then
                
            Dim IsLastOp
            IsLastOp = False
            
            If IsNumeric(bomStepClsP.nextOperation_) Then
        If bomStepClsP.nextOperation_ = 0 Then
                    IsLastOp = True
                End If
            End If    
            
            If IsLastOp Then
'KV2EP.en
'KV2EP.o    If bomStepClsP.nextOperation_ = 0 Then
            ' NextOperation = 0 means: last operation ---> Supplying step!
            Set supplyingStepP = bomStepClsP.bomStepP_
        Else
            Dim nextStepP
            Set nextStepP = FindBOMStep(bomStepClsP.nextOperation_, prmBomStepDict)
            If Not nextStepP Is bomStepClsP.bomStepP_ Then
                nextStepP.soStepPredPV.Append bomStepClsP.bomStepP_
            End If
        End If
        End If    'KV2EP.n
    Next

    If supplyingStepP Is Nothing Then
        ' No supplying step.  Force the MO to be deleted by the business rules.
        ITKLogError UE_TYPE_DELETE, prmSrcP, prmDstP, osMsgFileP.FormatMessage(PL_MSG_NO_SUPPLYING_STEP, PLGetDefaultSite(null))
        ' This is a trick for the MO to be deleted by the business rules
        prmDstP.quantity = 0.0
        prmDstP.status   = SOS_Planned
        CreateRelations  = False
        Exit Function
    End If

    ' Set the supplying step
    supplyingStepP.FK_plannedSupplyOrderSupplyingP = prmDstP
    supplyingStepP.SaveObject()

    ' Recalculate the MO due date, because the MO due date must match the supplying step due date
    Dim moDueDate
    moDueDate = Null

    ' Create the supplying transaction if not already there.
    ' The MO quantity must be equal the the supplying transaction quantity
    Dim supTrnP
    Set supTrnP = CreateSOStepTransaction(gMoItemP, supplyingStepP, 0, BAAN_TC_KOTR_PLANNED_RECEIPT, prmDstP.quantity, moDueDate, prmDstP)

    If Not supTrnP Is Nothing Then
        ' supTrnP will be nothing if the MO quantity is zero
        prmDstP.quantity = supTrnP.quantity
    End If

    CreateRelations = True
End Function

'****************************************************************************
'f Create a transaction for a manufacturing order.  The transaction may be
'  of type ToProduce or ToConsume.   If a transactionP of the same type already
'  exists in the database for the same item and operationP, we add the quantities.
'
'p prmWarehouseItemP    [ITKObject] Transaction's associated item in Planner
'p prmSOStepP           [ITKObject] SOStep of the MO, where to link the transaction
'p prmPosition          [Integer]   Position number of the estimated material
'p prmQty               [Double]    Transaction quantity
'p prmDate              [DateTime]  Transaction date
'p prmDstP               [ITKObject] Planner MO
'p prmBomQty            [Double]    BOM quantity
'p prmBomConstantQty    [Double]    Bom constant quantity
'
'r [ITKObject] The transaction created (may be Nothing if the quantity is 0)
'****************************************************************************
Private Function CreateSOStepTransaction(prmWarehouseItemP, prmSOStepP, prmPosition, prmTransactionType, _
                                         ByVal prmQty, prmDate, prmDstP)
    Dim transactionP
    Set transactionP = Nothing

    ' Compute the transactionP type
    
    Dim trnType, suffix
    If (prmTransactionType = BAAN_TC_KOTR_PLANNED_RECEIPT And prmQty > 0) Or (prmTransactionType = BAAN_TC_KOTR_PLANNED_ISSUE And prmQty < 0) Then
        trnType = TT_ToProduce
        Set transactionP = gBOSOStepSupplyTransactionP.NewObject()
        suffix = "P"
    Else
        trnType = TT_ToConsume
        Set transactionP = gBOSOStepDemandTransactionP.NewObject()
        suffix = "C"
    End If
    
    prmQty = Abs(prmQty)

    ' Build the code (EX: SFC000001+++ITEM+++010+++10+++P)
    transactionP.code = prmDstP.code & osGenSep & prmWarehouseItemP.code & osGenSep & _
                        prmSOStepP.code & osGenSep & prmPosition & osGenSep & suffix
    transactionP.FK_warehouseItemP  = prmWarehouseItemP
    transactionP.type               = trnType
    transactionP.quantity           = prmQty
    transactionP.FK_soStepP         = prmSOStepP
    transactionP.date               = prmDate

    ' A BOM with zero quantity will lead to an error...
    Dim bomItemP
    Set bomItemP = gBOSpecializedBOMItemP.NewObject()
    bomItemP.code               = transactionP.code
    bomItemP.quantity           = prmQty / prmDstP.Quantity
    'bomItemP.constantQuantity   = prmBomConstantQty
    bomItemP.FK_warehouseItemP  = prmWarehouseItemP

    If trnType = TT_ToProduce Then
        bomItemP.quantityQualifier  = BQQ_SO
        bomItemP.qualifierType      = QT_Production
    Else
        bomItemP.quantityQualifier  = BQQ_SOStep
        bomItemP.qualifierType      = QT_Consumption
    End If
    bomItemP.SaveObject()

    transactionP.FK_bomItemP = bomItemP

    transactionP.SaveObject()

    Set CreateSOStepTransaction = transactionP

End Function

'****************************************************************************
'f Find a routing in the Planner database, from a Baan routing code
'
'p prmSrcP          [ITKObject] Baan production order object, for error logging only
'p prmItem          [String]    Baan item code of the production order
'p prmCluster       [String]    Cluster of the production order item default warehouse
'p prmRoutingCode   [String]    Baan routing code of the production order
'
'r [ITKObject] Planner Manufacturing Process or Nothing if not found
'****************************************************************************
Private Function FindRouting(prmSrcP, prmItem, ByVal prmCluster, ByVal prmRoutingCode)
    prmRoutingCode = Trim(prmRoutingCode)

    Set FindRouting = Nothing
    If prmRoutingCode = "" Then
        Exit Function
    End If
    If Not (e2pSiteAsEU Or e2pClusterPlanning) Then
        prmCluster = ""
    End If
    ' Build the planner routing code
    Dim routingCode
    routingCode = E2PBaanNormalItemToProcessItemCode(prmItem, prmCluster) & osGenSep & _
                         prmRoutingCode & osGenSep & erpCompany

    If gRoutingDict.Exists(routingCode) Then
        Set FindRouting = gRoutingDict.Item(routingCode)
    Else
        ' cls is set in OnStartDataTransfer        
        plBOManufacturingProcessPrimaryP("code") = routingCode
        Set FindRouting = plBOManufacturingProcessP.FindObject(plBOManufacturingProcessPrimaryP)
        If Not FindRouting Is Nothing Then
            gRoutingDict.Add routingCode, FindRouting
        Else
            'Do not log when cluster planning and supply method is different than production
            'If gmoItemP.ERpdefaultsupplymethod = PL_DSM_MANUFACTURING Or Not e2pClusterPlanning Then
            '    ITKLogWarning UE_TYPE_UNKNOWN, prmSrcP, Null, osMsgFileP.FormatMessage(PL_MSG_CANT_LINK_TO_ROUTING, PLGetDefaultSiteCode(E2PGetEU(prmSrcP.EnterpriseUnit)), routingCode)
            'End If                
        End If
    End If
End Function

'****************************************************************************
'f Check if prmSourcingProcessP exists in prmWarehouseItemP.sourcingProcessPV
'
'p prmWarehouseItemP    [ITKObject] Planner warehouse item
'p prmSourcingProcessP  [ITKObject] Planner sourcing process
'
'r [Boolean]
'****************************************************************************
Private Function IsInProcessList(prmWarehouseItemP, prmSourcingProcessP)
    Dim sourcingProcessP
    For Each sourcingProcessP In prmWarehouseItemP.sourcingProcessPV
        If sourcingProcessP.id = prmSourcingProcessP.id Then
            IsInProcessList = True
            Exit Function
        End If
    Next
     ' Not found
    IsInProcessList = False
End Function

'****************************************************************************
'f Find an MOStep in the BomStep dictionary, given an operation number and a MO.
'  If we have MOStep 10, 30, 40 in the database and we are searching for operationP 20,
'  we return 30.  If the MO has no MOSteps, we assume that the given MO have at least one BOMStep.
'  Normally, if there is no MOSteps, the given prmOperation should be "0".
'
'p prmOperation     [Integer]       Operation number to find
'p prmBomStepDict   [Dictionary]    Dictionary of BOM steps
'
'r [ITKObject] The BOBomStep found (never Nothing!)
'****************************************************************************
Private Function FindBOMStep(prmOperation, prmBomStepDict)
    Dim code
    code = BCPadWithZeros(prmOperation, 3)

    If prmBomStepDict.Exists(code) Then
        Set FindBOMStep = prmBomStepDict.Item(code).bomStepP_
    Else
        Dim bomStepClsP
        ' If not found, we use the next BOMStep
        For Each bomStepClsP In prmBomStepDict.Items
            Set FindBOMStep = bomStepClsP.bomStepP_
            If FindBOMStep.code > code Then
                ' Found: exit now!
                Exit Function
            End If
        Next
        ' If not found it will be the last step...
    End If
End Function

'****************************************************************************
's Log error message in case the plan item of the estimated material is not found in Baan.
'  This will result as an empty default warehouse 
'
'p prmSrcP              [ITKObject]     Record of the SFC order 
'p prmLine              [Integer]	Position number of the estimated material line
'p prmItemCode		[String]	The item code of the estimated material line
'p prmWarehouseCode	[String]	The warehousecode
'					SiteMapping = EnterpriseUnit -> the warehouse of the SFC Order
'					SiteMapping = Company -> the warehouse of the estimated material line
'****************************************************************************
Private Sub LogMissingPlanItemForEstMaterial(prmSrcP, ByVal prmLine, ByVal prmBaanItemCode, ByVal prmWarehouseCode)
    Dim site, warehouse, item, itemDesc, cluster, itemInfoP, error_msg

    On Error Resume Next
    ' Go back in Baan to find the missing information.
    ' Not optimal, but it is the price to pay for making errors.
    Set itemInfoP = ITKGetClass(erpConnectionP, "common.Items").FindObject("MethodName=ShowItemWarehouseInfoForPlanner,Item=" & _
                    BCAddQuotes(prmBaanItemCode) & ",Warehouse=" & BCAddQuotes(prmWarehouseCode))

    site        = Trim(itemInfoP.EnterpriseUnit)
    item        = Trim(prmBaanItemCode)
    itemDesc    = itemInfoP.ItemDescription
    cluster     = Trim(itemInfoP.Cluster)

    On Error Goto 0
    
    error_msg = e2pMsgFileP.FormatMessage(E2P_MSG_UNABLE_TO_TRANSFER_ESTIMATED_MATERIAL, site, prmLine, item, itemDesc, cluster)
    ITKLogError UE_TYPE_DELETE, prmSrcP, Null, error_msg

End Sub

'****************************************************************************
's Add a BOMStep to the dictionay
'****************************************************************************
Private Sub AddBOMStepEx(prmBomStepP, prmNextOperation, prmPrevOperation, prmBomStepDict)
    Dim bomStepClsP
    Set bomStepClsP = New BOMStepClass
    Set bomStepClsP.bomStepP_ = prmBomStepP
    bomStepClsP.nextOperation_ = prmNextOperation
    bomStepClsP.prevOperation_ = prmPrevOperation
    prmBomStepDict.Add prmBomStepP.code, bomStepClsP
End Sub


Public Function CalculateDueStartDate(prmDueDate, prmOperationDurationSec, prmResourceCode, prmSiteCode)

    'This function takes into account the calendars of the resource and the operatingEfficiency of the resource.
    'If the resource does not have a calendar linked then the default calendar of the site will be used.
    'If there is no calendar in the database or if the beginning of the calendar is reached then 24h/7days is assumed.
    
    Dim SiteP
    Set SiteP = PLGetDefaultSite(null)

    Dim resourceP
    Set resourceP = PLFindProductionResource(Trim(prmResourceCode), prmSiteCode)

    Dim calendarId, OperationDurationSec, i, key, periodP, NbPeriods, lastPeriodStartDate, seconds
    Dim operatingEfficiency

    CalculateDueStartDate    = Empty
    lastPeriodStartDate         = Empty

    calendarId          = 0
    operatingEfficiency = 1

    'First check if there is a calendar attached to the machine.
    If Not resourceP is Nothing Then

        operatingEfficiency     = resourceP.operatingEfficiency

        If resourceP.calendarP > 0 Then
            calendarId              = resourceP.calendarP
            LoadCalendar resourceP.calendarP, gCalendarDict
        End If
    End If            
            
    If calendarId = 0 Then
        If SiteP.defaultCalendarP > 0 Then
            calendarId = SiteP.defaultCalendarP
            LoadCalendar SiteP.defaultCalendarP, gCalendarDict
        End If    
    End If    

    OperationDurationSec = prmOperationDurationSec
    OperationDurationSec = OperationDurationSec / operatingEfficiency
 
    If calendarId > 0 Then
 
        NbPeriods = gCalendarDict.item(calendarId)
 
        'Scan all the periods of the calendar backward.
        For i = 0 to NbPeriods - 1
            key = calendarId & "-" & NbPeriods - i
            Set periodP = gCalendarDict.item(key)
            
            lastPeriodStartDate = periodP.startDate
            
            If periodP.startDate < prmDueDate Then
                If periodP.endDate > prmDueDate Then
                    seconds = DateDiff("s", periodP.startDate, prmDueDate)
                Else
                    seconds = DateDiff("s",periodP.startDate, periodP.endDate)     
                End If
                
                OperationDurationSec = OperationDurationSec - seconds
                
                If OperationDurationSec < 0 Then
                    'The operation start date falls within this period
                    CalculateDueStartDate = DateAdd("s", - OperationDurationSec, periodP.startDate)
                    Exit For                                            
                End If
            End If    
        Next
 
    End If

    If IsEmpty(CalculateDueStartDate) Then
        If IsEmpty(lastPeriodStartDate) Then
            lastPeriodStartDate = prmDueDate
        ElseIf lastPeriodStartDate > prmDueDate Then
            lastPeriodStartDate = prmDueDate
        End If    
    
        'At this point we will plan 24h 7 days.
        CalculateDueStartDate = DateAdd("s", - OperationDurationSec, lastPeriodStartDate)            
    End If    
    
    Set resourceP   = Nothing
    Set SiteP       = Nothing
    
End Function


Public Function CalculateDueFinishDate(prmDueStartDate, prmOperationDurationSec, prmResourceCode, prmSiteCode)

    'This function takes into account the calendars of the resource and the operatingEfficiency of the resource.
    'If the resource does not have a calendar linked then the default calendar of the site will be used.
    'If there is no calendar in the database or if the beginning of the calendar is reached then 24h/7days is assumed.
    
    Dim SiteP
    Set SiteP = PLGetDefaultSite(null)

    Dim resourceP
    Set resourceP = PLFindProductionResource(Trim(prmResourceCode), prmSiteCode)

    CalculateDueFinishDate      = Empty
    lastPeriodFinishDate            = Empty

    Dim calendarId, OperationDurationSec, i, key, periodP, NbPeriods, lastPeriodFinishDate, seconds
    Dim operatingEfficiency

    CalculateDueFinishDate      = Empty
    lastPeriodFinishDate            = Empty

    calendarId              = 0
    operatingEfficiency     = 1

    'First check if there is a calendar attached to the machine.
    If Not resourceP is Nothing Then
        
        operatingEfficiency     = resourceP.operatingEfficiency
        
        If resourceP.calendarP > 0 Then
            calendarId = resourceP.calendarP
            LoadCalendar resourceP.calendarP, gCalendarDict
        End If
        
    End If            
            
    If calendarId = 0 Then
        If SiteP.defaultCalendarP > 0 Then
            calendarId = SiteP.defaultCalendarP
            LoadCalendar SiteP.defaultCalendarP, gCalendarDict
        End If    
    End If    

    OperationDurationSec = prmOperationDurationSec
    OperationDurationSec = OperationDurationSec / operatingEfficiency
 
    If calendarId > 0 Then
 
        NbPeriods = gCalendarDict.item(calendarId)
 
        'Scan all the periods of the calendar forward.
        For i = 1 to NbPeriods
            key = calendarId & "-" & i
            Set periodP = gCalendarDict.item(key)
            
            lastPeriodFinishDate = periodP.endDate
            
            If periodP.endDate > prmDueStartDate Then
                
                If periodP.startDate < prmDueStartDate Then
                    seconds = DateDiff("s", prmDueStartDate, periodP.endDate)
                Else
                    seconds = DateDiff("s", periodP.startDate, periodP.endDate)     
                End If
                
                OperationDurationSec = OperationDurationSec - seconds
                
                If OperationDurationSec < 0 Then
                    'The operation finsh date falls within this period
                    CalculateDueFinishDate = DateAdd("s", OperationDurationSec, periodP.endDate)
                    Exit For                                            
                End If
            End If    
        Next
 
    End If

    If IsEmpty(CalculateDueFinishDate) Then
        If IsEmpty(lastPeriodFinishDate) Then
            lastPeriodFinishDate = prmDueStartDate
        ElseIf lastPeriodFinishDate < prmDueStartDate Then
            lastPeriodFinishDate = prmDueStartDate
        End If    
    
        'At this point we will plan 24h 7 days.
        CalculateDueFinishDate = DateAdd("s", - OperationDurationSec, lastPeriodFinishDate)            
    End If    
    
    Set resourceP   = Nothing
    Set SiteP       = Nothing
    
End Function

Public Sub LoadCalendar(prmCalendarId, prmgCalendarDict)
    If Not prmgCalendarDict.Exists(prmCalendarId) Then

        Dim condition, i, Period
        condition = "calendarP = " & prmCalendarId
        
        i = 0
        
        For Each Period In plBOPeriodP.GetObjects(condition, "startDate")
            i = i + 1
            prmgCalendarDict.Add prmCalendarId & "-" & i, Period
        Next
    
        'Store the numbers of periods for the calendar.
        prmgCalendarDict.Add prmCalendarId, i
    End If
End Sub