'****************************************************************************
' %name:           EnterprisePlanning.PlannedInventoryMovementsByItem-BOPurchaseOrder.vbs %
' %version:        1 %
' %created_by:     groussea %
' %derived_by:     groussea %
' %date_modified:  %
'****************************************************************************
Option Explicit

Dim gPOItemP

Dim gEPFromStatusFilter
Dim gEPPrefix

'****************************************************************************
's Called once at the beginning of the transfer, before getting any data from the source
'
'p prmClassSrcP [ITKClass]  Object that represents the source class
'p prmClassDstP [ITKClass]  Object that represents the destination class
'****************************************************************************
Public Sub OnStartDataTransfer(prmClassSrcP, prmClassDstP)
    ITKValidateConfiguration prmClassSrcP, "EnterprisePlanning.PlannedInventoryMovementsByItemEx", _
                             prmClassDstP, "BOPurchaseOrder", UE_MATCH_MANUAL, -1, UE_TRANSFER_RESYNC Or UE_TRANSFER_CRC

    E2PStartDataTransfer()
    
    gEPFromStatusFilter                  = ITKReadProperty(Null, "EPFromStatusFilter", "10", Null, Null, Null)
    gEPPrefix                            = ITKReadProperty(Null, "EPPrefix", "EP", Null, Null, Null)    
    
End Sub

'****************************************************************************
's Called once at the end of the transfer.
'
'p prmClassSrcP [ITKClass]  Object that represents the source class
'p prmClassDstP [ITKClass]  Object that represents the destination class
'****************************************************************************
Public Sub OnTerminateDataTransfer(prmClassSrcP, prmClassDstP)
    Set gPOItemP = Nothing
    E2PTerminateDataTransfer()
End Sub

'****************************************************************************
'f Called to allow the scripter to specify the source filter
'
'p prmClassSrcP     [ITKClass]  Source class
'p prmCurrentFilter [String]    The current filter in ITKManager
'
'r [String] The new computed filter
'****************************************************************************
Function OnGetSourceFilter(prmClassSrcP, prmCurrentFilter)
	
    OnGetSourceFilter = ERPGetPlannerFilter("ListPlannedPurchaseOrdersForPlanner", "Scenario=" & BCAddQuotes(ERPGetEPParameters().ActualScenario), False)
    OnGetSourceFilter = OnGetSourceFilter & ",FromOrderStatusE2P=" & gEPFromStatusFilter
    
End Function

'****************************************************************************
'f Called to allow the scripter to specify the target filter
'
'p prmClassDstP     [ITKClass]  Target class
'p prmCurrentFilter [String]    The current filter in ITKManager
'
'r [String] The new computed filter
'****************************************************************************
Function OnGetTargetFilter(prmClassDstP, prmCurrentFilter)
    If Not e2pSiteAsEU Then
        OnGetTargetFilter = "warehouseItemP.warehouseP.siteP = " & PLGetCurrentSite().id & " AND "
    End If

    OnGetTargetFilter = OnGetTargetFilter & "code % " & BCAddQuotes(gEPPrefix & ".*")
    
End Function

'****************************************************************************
'f Called to allow the scripter to find the object in the
'  destination application. So the object will updated instead of inserted.
'
'p prmSrcP [ITKObject]  Object that represents the current record from the source
'
'r [ITKObject | UE_SKIP_RECORD] Object found in the destination or Nothing if the object must be inserted
'****************************************************************************
Function OnManualMatch(prmSrcP)
    OnManualMatch = UE_SKIP_RECORD
 
    Set gPOItemP = E2PFindItem(prmSrcP, prmSrcP.PlanItem, Empty, Empty, Null)
    If gPOItemP Is Nothing Then
        Exit Function
    End If

    Dim code
    code = gEPPrefix & osgensep & Trim(prmSrcP.PlannedOrder)

    If prmSrcP.LinePositionNumber <> 10 Then
        code = code & osgensep & prmSrcP.LinePositionNumber
    End If    
    
    If prmSrcP.PositionNumber > 0 Then
        code = code & osgensep & prmSrcP.PositionNumber
    End If    

    plBOPurchaseOrderPrimaryP("warehouseItemP") = gPOItemP
    plBOPurchaseOrderPrimaryP("code") = code
    ' cls is set in OnStartDataTransfer
    Set OnManualMatch = plBOPurchaseOrderP.FindObject(plBOPurchaseOrderPrimaryP)
End Function

'****************************************************************************
'f Called once for each record from the source,
'  after the automatic copy performed by the ITK Server
'
'p prmSrcP          [ITKObject]  Object that represents the current
'p prmDstP          [ITKObject]  Object that represents the record
'                                that will be saved in the destination
'p prmIsNewObject   [Boolean]    True if the destination object does not
'
'r UE_OK:           Ok. Continue to the next step
'  UE_SKIP_RECORD:  Skip the record. It will not be saved in the destination
'****************************************************************************
Public Function OnAfterCopy(prmSrcP, prmDstP, prmIsNewObject)
    OnAfterCopy = UE_SKIP_RECORD
    
    If prmIsNewObject Then
        ' The code was already formed in OnManualMatch
        prmDstP.code = plBOPurchaseOrderPrimaryP("code")
    Else
        prmDstP.batchUpdateIfExists = True
    End If    

    prmDstP.purchasePrice       = 0

    prmDstP.dueFinishDate       = #2040-01-01#
    prmDstP.dueDate             = prmSrcP.Date
    prmDstP.FK_warehouseItemP   = gPOItemP
    prmDstP.quantity            = prmSrcP.Quantity
    
    Select Case prmSrcP.OrderStatus
        Case BAAN_CP_RRP_OSTA_PLANNED
            prmDstP.Status          =  SOS_Planned
            prmDstP.EPStatus      	=  SOS_Planned
        Case BAAN_CP_RRP_OSTA_FIRM_PLANNED
            'prmDstP.Status         =  SOS_FirmPlanned
            prmDstP.EPStatus      	=  SOS_FirmPlanned
        Case BAAN_CP_RRP_OSTA_CONFIRMED
            'prmDstP.Status         =  SOS_Confirmed
            prmDstP.EPStatus      	=  SOS_Confirmed
    End Select

    'The right status will be set back after the REQ engine with a script.    
    prmDstP.Status          =  SOS_Released

    ' Buy from supplier
    Dim supplierBuyFrom    
    supplierBuyFrom = Trim(prmSrcP.Supplier)
    
    If supplierBuyFrom = "" Then
        If e2pBlankSupplierCode <> "" Then
            supplierBuyFrom = e2pBlankSupplierCode
        End If    
    End If
    
    If supplierBuyFrom <> "" Then
        Dim supplierP
        Set supplierP = PLFindSupplier(supplierBuyFrom)
        If Not supplierP Is Nothing Then
            prmDstP.FK_supplierBuyFromP = supplierP
        Else
            ITKLogWarning UE_TYPE_UNKNOWN, prmSrcP, Null, osMsgFileP.FormatMessage(PL_MSG_SUPPLIER_NOT_FOUND, PLGetDefaultSiteCode(E2PGetEU(prmSrcP.enterpriseUnit)), supplierBuyFrom)
        End If
    End If
    
    ' Ship from supplier
    Dim SupplierShipFrom
    
    SupplierShipFrom = Trim(prmSrcP.SupplierShipFrom)
    
    If SupplierShipFrom = "" Then
        If e2pBlankSupplierCode <> "" Then
            SupplierShipFrom = e2pBlankSupplierCode
        End If    
    End If
    
    If SupplierShipFrom <> "" Then
        Set supplierP = PLFindSupplier(SupplierShipFrom)
        If Not supplierP Is Nothing Then
            prmDstP.FK_supplierShipFromP = supplierP
        Else
            ITKLogWarning UE_TYPE_UNKNOWN, prmSrcP, Null, osMsgFileP.FormatMessage(PL_MSG_SUPPLIER_NOT_FOUND, PLGetDefaultSiteCode(E2PGetEU(prmSrcP.enterpriseUnit)), SupplierShipFrom)
        End If
    End If

    If plVersion >= "2.4.0" Then
        ' Set buyer.  Don't bother if not found.
        prmDstP.FK_actualBuyerP = PLFindWarehouseItemGroup(E2PBuildWIGCode(e2pEmployeePrefix, prmSrcP.Planner), CT_BOBuyer, True)
    End If
    
    prmDstP.SaveObject()
    ITKLogOk ITKGetTrnType(prmIsNewObject), prmSrcP, prmDstP, ""
End Function


