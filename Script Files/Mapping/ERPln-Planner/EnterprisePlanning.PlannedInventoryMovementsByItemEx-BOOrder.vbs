'****************************************************************************
' %name:           EnterprisePlanning.PlannedInventoryMovementsByItem-BOOrder.vbs %
' %version:        1 %
' %created_by:     groussea %
' %derived_by:     groussea %
' %date_modified:   %
'****************************************************************************
Option Explicit

Dim gBOOrderP
Dim gBOOrderPrimaryP
Dim gToItemP
Dim gFromItemP
Dim gTargetCls

Dim gEPFromStatusFilter
Dim gEPPrefix

'****************************************************************************
's Called once at the beginning of the transfer, before getting any data from the source
'
'p prmClassSrcP [ITKClass]  Object that represents the source class
'p prmClassDstP [ITKClass]  Object that represents the destination class
'****************************************************************************
Public Sub OnStartDataTransfer(prmClassSrcP, prmClassDstP)
   ITKValidateConfiguration prmClassSrcP, "EnterprisePlanning.PlannedInventoryMovementsByItemEx", prmClassDstP, "BOOrder", _
                            UE_MATCH_MANUAL, Null, UE_TRANSFER_RESYNC Or UE_TRANSFER_CRC

    E2PStartDataTransfer()

    Set gBOOrderP        = prmClassDstP
    Set gBOOrderPrimaryP = gBOOrderP.Keys("Primary").CreateKeyFields
    
    gEPFromStatusFilter                  = ITKReadProperty(Null, "EPFromStatusFilter", "10", Null, Null, Null)
    gEPPrefix                            = ITKReadProperty(Null, "EPPrefix", "EP", Null, Null, Null)

    'osConnectionP.Properties("BufferingMode") = "ByRecord"    
    
End Sub

'****************************************************************************
's Called once at the end of the transfer.
'
'p prmClassSrcP [ITKClass]  Object that represents the source class
'p prmClassDstP [ITKClass]  Object that represents the destination class
'****************************************************************************
Public Sub OnTerminateDataTransfer(prmClassSrcP, prmClassDstP)
    Set gBOOrderP               = Nothing
    Set gBOOrderPrimaryP        = Nothing
    Set gToItemP                = Nothing
    Set gFromItemP              = Nothing

    E2PTerminateDataTransfer()
End Sub

'****************************************************************************
'f Called to allow the scripter to specify the source filter
'
'p prmClassSrcP     [ITKClass]  Source class
'p prmCurrentFilter [String]    The current filter in ITKManager
'
'r [String] The new computed filter
'****************************************************************************
Function OnGetSourceFilter(prmClassSrcP, prmCurrentFilter)
    ' Transfer order:  will create distribution and substitution orders
    OnGetSourceFilter = _
        ERPGetPlannerFilter("ListPlannedDistributionOrdersForPlanner", "Scenario=" & BCAddQuotes(ERPGetEPParameters().ActualScenario), False)
        OnGetSourceFilter = OnGetSourceFilter & ",FromOrderStatusE2P=" & gEPFromStatusFilter

End Function

'****************************************************************************
'f Called to allow the scripter to specify the target filter
'
'p prmClassDstP     [ITKClass]  Target class
'p prmCurrentFilter [String]    The current filter in ITKManager
'
'r [String] The new computed filter
'****************************************************************************
Function OnGetTargetFilter(prmClassDstP, prmCurrentFilter)

    OnGetTargetFilter = OnGetTargetFilter & "code % " & BCAddQuotes(gEPPrefix & ".*")
    OnGetTargetFilter = OnGetTargetFilter & " AND NOT cls = " & CT_BOPurchaseOrder & " AND NOT cls = " & CT_BOManufacturingOrder
    
End Function

'****************************************************************************
'f Called to allow the scripter to find the object in the
'  destination application. So the object will updated instead of inserted.
'
'p prmSrcP [ITKObject]  Object that represents the current record from the source
'
'r [ITKObject | UE_SKIP_RECORD] Object found in the destination or Nothing if the object must be inserted
'****************************************************************************
Function OnManualMatch(prmSrcP)
    OnManualMatch = UE_SKIP_RECORD
    
    Dim cluster, supplyingCluster    

    ERPGetBaanItem().SetPlanningItem()
    ERPGetBaanItem().SetBaanItem(prmSrcP.SupplyingPlanItem)
    supplyingCluster = Trim(ERPGetBaanItem().GetCluster())    

    ERPGetBaanItem().SetPlanningItem()
    ERPGetBaanItem().SetBaanItem(prmSrcP.PlanItem)
    cluster = Trim(ERPGetBaanItem().GetCluster())

    ERPGetBaanItem().SetPlanningItem()
    Set gToItemP = E2PFindItem(prmSrcP, prmSrcP.PlanItem, Empty, Empty, Null)
    
    ERPGetBaanItem().SetPlanningItem()
    Set gFromItemP = E2PFindItem(prmSrcP, prmSrcP.SupplyingPlanItem, prmSrcP.SupplyingDefaultWarehouse, E2PGetEU(prmSrcP.SupplyingEnterpriseUnit), Null)
    
    If gFromItemP Is Nothing And gToItemP Is Nothing Then
        'Nothing to do
        Exit Function
    End If    

    If Not gFromItemP Is Nothing And Not gToItemP Is Nothing Then
        ' Determine what will be the target class
        
        ' Decode the item code and project
        Dim FromItemCode, FromProjectCode, ToItemCode, ToProjectCode
        ERPGetBaanItem().ResetAll()
        ERPGetBaanItem().SetBaanItem(prmSrcP.PlanItem) 
        FromItemCode = Trim(ERPGetBaanItem().GetItem())
        FromProjectCode = Trim(ERPGetBaanItem().GetProject())
       
        ERPGetBaanItem().ResetAll()
        ERPGetBaanItem().SetBaanItem(prmSrcP.SupplyingPlanItem) 
        ToItemCode = Trim(ERPGetBaanItem().GetItem())
        ToProjectCode = Trim(ERPGetBaanItem().GetProject())
        
        If FromItemCode = ToItemCode And FromProjectCode = ToProjectCode Then
            ' Same item
        '    If prmSrcP.ToDefaultWarehouse = prmSrcP.FromDefaultWarehouse Then
        '        Exit Function
        '    Else
                gTargetCls = CT_BODistributionOrder
        '    End If
        Else
            ' Different item
            gTargetCls = CT_BOSubstitutionOrder
        End If
    ElseIf Not gToItemP Is Nothing Then
        gTargetCls = CT_BOWarehouseSupplyOrder
    Else    
        gTargetCls = CT_BOWarehouseDemandOrder
    End If
    
    ' Build code
    Dim code
    code = gEPPrefix & osgensep & Trim(prmSrcP.PlannedOrder)
    
    ' Find the order
    gBOOrderPrimaryP("cls")             = gTargetCls
    
    If gTargetCls = CT_BOWarehouseDemandOrder Then
        gBOOrderPrimaryP("warehouseItemP") = gFromItemP
    Else
        gBOOrderPrimaryP("warehouseItemP") = gToItemP
    End If
    
    gBOOrderPrimaryP("code")            = code
    Set OnManualMatch = gBOOrderP.FindObject(gBOOrderPrimaryP)
    
    'The bellow code assume that if a warehouse demand order has a splitDelivery = 1 or 2 then it does not have a splitDelivery = 0
    If plVersion >= "2.4.3" Then
        If gTargetCls = CT_BOWarehouseDemandOrder Then
            If Not OnManualMatch is Nothing Then
                If OnManualMatch.splitDelivery <> 0 Then
                    Set OnManualMatch = Nothing
                End If    
            End If
        End If    
    End If    
        
End Function


'****************************************************************************
'f User exit to let ITK Server know what is the exact target class.
'
'p prmSrcP [ITKObject]  Source object
'r name of destination class.
'****************************************************************************
Public Function OnGetTargetClass(prmSrcP)
    Select Case gTargetCls
        Case CT_BOSubstitutionOrder
            OnGetTargetClass = "BOSubstitutionOrder"
        Case CT_BODistributionOrder
            OnGetTargetClass = "BODistributionOrder"
        Case CT_BOWarehouseSupplyOrder
            OnGetTargetClass = "BOWarehouseSupplyOrder"
        Case CT_BOWarehouseDemandOrder
            OnGetTargetClass = "BOWarehouseDemandOrder"            
    End Select
End Function

'****************************************************************************
'f Called once for each record from the source,
'  after the automatic copy performed by the ITK Server
'
'p prmSrcP          [ITKObject]  Object that represents the current
'p prmDstP          [ITKObject]  Object that represents the record
'                                that will be saved in the destination
'p prmIsNewObject   [Boolean]    True if the destination object does not
'
'r UE_OK:           Ok. Continue to the next step
'  UE_SKIP_RECORD:  Skip the record. It will not be saved in the destination
'****************************************************************************
Public Function OnAfterCopy(prmSrcP, prmDstP, prmIsNewObject)
    OnAfterCopy = UE_SKIP_RECORD

    If prmIsNewObject Then
        prmDstP.code              = gBOOrderPrimaryP("code")
        
        If gTargetCls = CT_BOWarehouseDemandOrder Then
            prmDstP.FK_warehouseItemP = gFromItemP
            
            If plVersion >= "2.4.3" Then
                prmDstP.splitDelivery = PL_SPLITDELIVERY_DEFAULT
            End If
        Else
            prmDstP.FK_warehouseItemP = gToItemP
        End If    
    Else
        prmDstP.batchUpdateIfExists = True
    End If

    prmDstP.quantity        = prmSrcP.Quantity

    If gTargetCls = CT_BOWarehouseDemandOrder Then
        prmDstP.dueDate     = prmSrcP.SupplyingDate
    Else
        prmDstP.dueDate     = prmSrcP.Date
    End If

    If gTargetCls = CT_BODistributionOrder Or gTargetCls = CT_BOSubstitutionOrder Then
        prmDstP.dueFinishDate       = #2040-01-01#
        prmDstP.dueStartDate        = prmSrcP.SupplyingDate            

        Select Case prmSrcP.OrderStatus
            Case BAAN_CP_RRP_OSTA_PLANNED
                'prmDstP.Status             =  SOS_Planned
                prmDstP.EPStatus      =  SOS_Planned
                'prmDstP.EPStatusText  =  "Planned"
            Case BAAN_CP_RRP_OSTA_FIRM_PLANNED
                'prmDstP.Status             =  SOS_FirmPlanned
                prmDstP.EPStatus      =  SOS_FirmPlanned
                'prmDstP.EPStatusText  =  "Firm Planned"
            Case BAAN_CP_RRP_OSTA_CONFIRMED
                'prmDstP.Status             =  SOS_Confirmed
                prmDstP.EPStatus      =  SOS_Confirmed
                'prmDstP.EPStatusText  =  "Confirmed"
        End Select
    
        'The right status will be set back after the REQ engine with a script.    
        prmDstP.Status          =  SOS_Released        
    End If        
   	
    Select Case gTargetCls
        Case CT_BODistributionOrder
            prmDstP.FK_warehouseItemDistributionOriginP = gFromItemP
			prmDstP.transferTime  =  DateDiff("s", prmSrcP.SupplyingDate, prmSrcP.Date)

        Case CT_BOSubstitutionOrder
            prmDstP.FK_warehouseItemSubstitutForP = gFromItemP
            
        Case CT_BOWarehouseDemandOrder
            prmDstP.comment  =  "Demand EU:'" & prmSrcP.EnterpriseUnit & "'  " & _
                                "Demand Plan item:'" & prmSrcP.PlanItem & "'  " & _
                                "Demand Warehouse:'" & prmSrcP.DefaultWarehouse & "'"

        Case CT_BOWarehouseSupplyOrder
            prmDstP.comment  =  "Supplying EU:'" & prmSrcP.SupplyingEnterpriseUnit & "'  " & _
                                "Supplying Plan item:'" & prmSrcP.SupplyingPlanItem & "'  " & _
                                "Supplying Warehouse:'" & prmSrcP.SupplyingDefaultWarehouse & "'"
    End Select
    
    If plVersion >= "2.4.0" Then
        ' Set buyer.  Don't bother if not found.
        If gTargetCls = CT_BODistributionOrder Or gTargetCls = CT_BOSubstitutionOrder Then
            prmDstP.FK_actualBuyerP = PLFindWarehouseItemGroup(E2PBuildWIGCode(e2pEmployeePrefix, prmSrcP.Planner), CT_BOBuyer, True)
        End If    
    End If
    
    prmDstP.SaveObject()
    ITKLogOk ITKGetTrnType(prmIsNewObject), prmSrcP, prmDstP, ""
End Function
